import yaml
import pathlib
import os.path

import numpy as np
from pandas import read_csv as pdreadcsv

from train import Train
from eval import Eval
from check_tools import CheckTools
from data_tools import DataTools


class TelsipBackend:
    def __init__(self, config, mateng, lock, logger):
        self.lock = lock
        with self.lock:
            paths = open('paths.yaml', "r")
            path_dict = yaml.safe_load(paths.read())
        model_paths = path_dict.get('model_paths')
        trained_model_paths = path_dict.get('trained_model_paths')
        dataset_paths = path_dict.get('dataset_paths')
        scenario_paths = path_dict.get('scenario_paths')
        model_infos = path_dict.get('model_info')
        paths.close()

        config['model_paths'] = model_paths
        config['trained_model_paths'] = trained_model_paths
        config['dataset_paths'] = dataset_paths
        config['scenario_paths'] = scenario_paths
        config['model_infos'] = model_infos
        config['server_logger'] = logger

        self.timestamp = config.get('request_timestamp')
        self.config = config
        self.server_logger = logger
        self.mateng = mateng

    def train(self):
        """
        Create a new trained model, given a Model ID and a training dataset. Store the trained model. Update YAML.
        """
        # Check structure: the backend sequentially checks all conditions that are necessary for train to take place,
        # namely if the model, scenario, and substation IDs exist, and if the dataset-scenario merge has been
        # successful.
        model_id = self.config.get('model_id')
        train_scenario_id = self.config.get('train_scenario')  # Get train scenario ID (expected: "scXXX_YY")
        substation_id = self.config.get('substation_id')
        log_id = 'Train Request {!s}_{!s}_{!s}: '.format(model_id, train_scenario_id,
                                                         substation_id)
        self.config['logger_sentence'] = log_id

        # Initialize all interfaces
        check_tools = CheckTools(self.config, self.server_logger)
        data_tools = DataTools(self.config, self.mateng, self.server_logger, 'Train')
        train_interface = Train(self.config, self.mateng, check_tools, data_tools)

        check_msg = check_tools.check_train()            # This checks if the requested items exist.
        if check_msg == 'Model_ID_+_scenario_ID_+_subst_ID_exist':
            try:
                output = data_tools.merge_data(None)    # This will either return the dataset or an error msg
                if isinstance(output, str):
                    return_msg = output                         # Return error msg
                else:
                    return_msg = train_interface.train(output)   # Commence training
                    if return_msg == 'Train Complete':
                        # Update Yaml
                        with self.lock:
                            return_msg = train_interface.update_yaml()
                    else:
                        return_msg = check_msg
            except RuntimeError:
                return_msg = "{!s} unknown_exception_occurred".format(log_id)
                self.server_logger.warning('{!s} unknown_exception_occurred'.format(log_id))
                return {'output': return_msg}

        else:
            return_msg = check_msg
        return {'output': return_msg}

    def eval(self):
        """
        Evaluate an existing trained model, given a Trained Model ID, a substation and a testing dataset.
        """
        # Check structure: the backend sequentially checks all conditions that are necessary for eval to take place,
        # namely if the model, scenarios, and substation IDs exist, and if the dataset-scenario merge has been
        # successful.
        model_id = self.config.get('model_id')
        train_scenario_id = self.config.get('train_scenario')
        substation_id = self.config.get('substation_id')
        test_scenario_id = self.config.get('test_scenario')
        log_id = 'Eval Request {!s}_{!s}_{!s}_{!s}: '.format(model_id, train_scenario_id,
                                                             substation_id, test_scenario_id)
        self.config['logger_sentence'] = log_id

        # Initialize all interfaces
        check_tools = CheckTools(self.config, self.server_logger)
        data_tools = DataTools(self.config, self.mateng, self.server_logger, 'Eval')
        eval_interface = Eval(self.config, self.mateng, check_tools, data_tools)

        check_msg = check_tools.check_eval()
        if check_msg == 'Trained_Model_ID_+_scenario_ID_+_subst_ID_exist':
            try:
                output = data_tools.merge_data(None)    # This will either return the dataset or an error msg
                if isinstance(output, str):
                    output = output
                else:
                    output = eval_interface.eval(output)
            except RuntimeError:
                output = "{!s} unknown_exception_occurred".format(log_id)
                self.server_logger.warning(' {!s} unknown_exception_occurred'.format(log_id))
        else:
            output = check_msg

        return output

    def direct_eval(self, test_data):
        """
        Evaluate an existing trained model, given a Trained Model ID and a testing dataset.
        """
        # Check structure: the backend sequentially checks all conditions that are necessary for direct_eval to take
        # place, namely if the model, scenarios, and substation IDs exist, and if the dataset-scenario merge has been
        # successful.
        model_id = self.config.get('model_id')
        train_scenario_id = self.config.get('train_scenario')
        substation_id = self.config.get('substation_id')
        log_id = 'D-Eval Request {!s}_{!s}_{!s}: '.format(model_id, train_scenario_id,
                                                          substation_id)
        self.config['logger_sentence'] = log_id

        # Initialize all interfaces
        check_tools = CheckTools(self.config, self.server_logger)
        data_tools = DataTools(self.config, self.mateng, self.server_logger, 'direct_eval')
        eval_interface = Eval(self.config, self.mateng, check_tools, data_tools)

        check_msg = check_tools.check_eval()
        if check_msg == 'Trained_Model_ID_exists__Scenario_ID_nonexistent':
            try:
                output = data_tools.merge_data(test_data)
                if isinstance(output, str):
                    output = output
                else:
                    output = eval_interface.eval(output)
            except RuntimeError:
                output = "{!s} unknown_exception_occurred".format(self.timestamp)
                self.server_logger.warning('{!s} unknown_exception_occurred'.format(self.timestamp))
        else:
            output = check_msg
        return output

    def outlier_detection(self, od_config):
        """
        Find outliers in stored active power data and return a vector of the data with outliers highlighted.
        """
        import pandas as pd
        import matlab.engine
        import datetime as dt
        dataset_paths = self.config['dataset_paths']
        substation_data_path = dataset_paths.get(od_config['substation_id'])
        self.server_logger.warning('Outlier Detection Request {!s} : Substation {!s}'.format(self.timestamp,
                                                                                             substation_data_path))
        if substation_data_path is not None:

            # Read substation data, crop based on given datetime range.
            substation_data_path = '{!s}.csv'.format(substation_data_path)
            substation_data = pd.read_csv(substation_data_path, parse_dates=['SL_DateTime'])
            substation_data.set_index(substation_data['SL_DateTime'], inplace=True)
            daterange = [dt.datetime.strptime(od_config['date_from'], '%Y-%m-%d %H:%M:%S'),
                         dt.datetime.strptime(od_config['date_to'], '%Y-%m-%d %H:%M:%S')]
            substation_data = substation_data[(substation_data.index > daterange[0]) &
                                              (substation_data.index < daterange[1])]

            # Drop duplicate datetimes and re-sort on index (this is needed since matlab throws an error otherwise)
            substation_data = substation_data.loc[~substation_data.index.duplicated(keep='first')]
            substation_data.dropna(subset=['SL_DateTime'], inplace=True)
            substation_data.sort_index(inplace=True)

            # Prepare data for matlab evaluation
            matlab_ap_vector = matlab.double(substation_data['SL_Active'].values.tolist())
            matlab_datetime_vector = pd.to_timedelta(substation_data['SL_DateTime']).dt.total_seconds()
            matlab_datetime_vector = matlab.double(matlab_datetime_vector.values.tolist())

            # Evaluate data using outlierdetect.m
            matlabengine = self.mateng
            is_outlier = matlabengine.outlierdetect(matlab_ap_vector,
                                                    matlab_datetime_vector, od_config.get('od_method'),
                                                    od_config.get('mov_window'), od_config.get('threshold_factor'))
            is_outlier = np.asarray(is_outlier._data)                           # Convert to array
            outlier_filter = pd.Series(is_outlier, index=substation_data.index)

            # Add outlier filter column to existing Active power data, return them.
            output = substation_data[['SL_Active', 'SL_DateTime']]
            output['outlier_filter'] = outlier_filter

            self.server_logger.warning('OD Request {!s} : '
                                       'Successful Outlier Detection'.format(self.timestamp))
        else:
            output = "substation_id_nonexistent"
            self.server_logger.warning('OD Request {!s} : '
                                       'Unsuccessful Outlier Detection - Subst_ID not found'.format(self.timestamp))
        return output

    def data_grab(self):
        flag = self.config.get('preprocess_data')
        # This flag parameter enables preprocessing or not. The client may want just the merged dataset-scenario, and
        # not any preprocessing done
        model_id = self.config.get('model_id')
        train_scenario_id = self.config.get('train_scenario')
        substation_id = self.config.get('substation_id')
        log_id = 'Data Request {!s}_{!s}_{!s}: '.format(model_id, train_scenario_id,
                                                        substation_id)
        self.config['logger_sentence'] = log_id
        self.config['overwrite_model'] = False

        # Initialize all interfaces
        check_tools = CheckTools(self.config, self.server_logger)
        data_tools = DataTools(self.config, self.mateng, self.server_logger, 'Train')
        train_interface = Train(self.config, self.mateng, check_tools, data_tools)

        check_msg = check_tools.check_train()                      # This checks if the requested items exist.
        if (check_msg == 'Model_ID_+_scenario_ID_+_subst_ID_exist') | (check_msg == 'Trained_Model_ID_exists'):
            try:
                output = data_tools.merge_data(None)               # This will either return the dataset or an error msg
                if isinstance(output, str):
                    output = output                                      # Return error msg
                elif flag:
                    output = train_interface.preprocessed_data(output)   # Grab preprocessed data
            except RuntimeError:
                return_msg = "{!s} unknown_exception_occurred".format(log_id)
                self.server_logger.warning('{!s} unknown_exception_occurred'.format(log_id))
                return return_msg
        else:
            output = check_msg
        return output

    def res_grab(self):
        """
        Grab results.
        """
        model_id = self.config.get('model_id')
        train_scenario = self.config.get('train_scenario')
        subst_id = self.config.get('substation_id')
        test_scenario = self.config.get('test_scenario')
        trained_model_paths = self.config.get('trained_model_paths')
        start_date = np.datetime64(self.config.get('from'))
        end_date = np.datetime64(self.config.get('to'))
        run_id = '{!s}_{!s}_{!s}_{!s}'.format(model_id, train_scenario, subst_id, test_scenario)
        flag = 'nonexistent'
        if trained_model_paths.get(model_id) is not None:
            res_path = trained_model_paths.get(model_id)
            if res_path.get(train_scenario) is not None:
                res_path = res_path.get(train_scenario)
                if res_path.get(subst_id) is not None:
                    res_path.get(subst_id)
                    flag = 'exists'
        if flag == 'exists':
            res_path = trained_model_paths.get(model_id).get(train_scenario).get(subst_id)
            csv_path = './{!s}/res/{!s}.csv'.format(res_path, run_id)
            if os.path.isfile(csv_path):
                results = pdreadcsv(csv_path, parse_dates=['datetime'])
                date_filter = (results['datetime'] > start_date) & (results['datetime'] < end_date)
                output = results[date_filter]
            else:
                output = 'run_nonexistent'
        else:
            output = 'model_not_trained'
        self.server_logger.warning('Res Grab Request {!s} : Received @ {!s}, output {!s}'.format(run_id, self.timestamp,
                                                                                                 flag))
        return output

    @staticmethod
    def model_list():
        """
        Return a list of the trained models currently in the directory.
        """
        mf = pathlib.Path('paths.yaml')
        with open(mf) as fp:
            yaml_dict = yaml.safe_load(open(mf))
        output = {'model directory': yaml_dict}
        return output

    def store_dataset(self, dataset, dataset_name):
        """
        Store received dataset, update yaml.
        """
        path_to_assign = 'datasets/data/{!s}'.format(dataset_name)
        dataset.to_csv('{!s}.csv'.format(path_to_assign))
        # Update YAML
        mf = pathlib.Path('paths.yaml')
        with self.lock:
            with open(mf) as fp:
                yaml_dict = yaml.safe_load(fp)
            dataset_paths = yaml_dict.get('dataset_paths')
            dataset_paths[dataset_name] = path_to_assign
            with open('paths.yaml', 'w') as fp:
                yaml.safe_dump(yaml_dict, fp)
        msg = "Dataset path added succesfully"
        self.server_logger.warning('Store Dataset Request {!s} : Dataset added @ {!s}'.format(dataset_name,
                                                                                              self.timestamp))
        return msg

    def store_scenario(self, scenario, scenario_name):
        """
        Attach the cached scenario to a dataset, according to the config received.
        """
        path_to_assign = 'datasets/scenarios/{!s}'.format(scenario_name)
        scenario.to_csv('{!s}.csv'.format(path_to_assign))

        # Update YAML
        mf = pathlib.Path('paths.yaml')
        with self.lock:
            with open(mf) as fp:
                yaml_dict = yaml.safe_load(fp)
            scenario_paths = yaml_dict.get('scenario_paths')
            scenario_paths[scenario_name] = path_to_assign
            with open('paths.yaml', 'w') as fp:
                yaml.safe_dump(yaml_dict, fp)
        self.server_logger.warning('Store Scenario Request {!s} : Dataset added @ {!s}'.format(scenario_name,
                                                                                               self.timestamp))
        msg = "Scenario path added succesfully"
        return msg
