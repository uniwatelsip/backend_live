import importlib
import os
from shutil import rmtree
import yaml
import pathlib
from check_tools import CheckTools


class Train:
    def __init__(self, config, mateng, check_tools, data_tools):
        self.model_paths = config.get('model_paths')
        self.trained_model_paths = config.get('trained_model_paths')
        self.scenario_paths = config.get('scenario_paths')
        self.dataset_paths = config.get('dataset_paths')
        self.model_id = config.get('model_id')
        self.train_scenario_id = config.get('train_scenario')
        self.substation_id = config.get('substation_id')
        self.model_path = self.model_paths.get(self.model_id)
        self.model_infos = config.get('model_infos')
        self.model_info = self.model_infos.get(self.model_id)
        resample_config = self.model_info.get('resample_config')
        resample_config['active_columns'] = self.model_info.get('inputs')
        self.model_info['resample_config'] = resample_config
        self.overwrite_model = config.get('overwrite_model')
        self.matlabeng = mateng
        self.logger_sentence = config.get('logger_sentence')
        config['logger_sentence'] = self.logger_sentence
        self.server_logger = config.get('server_logger')

        self.data_tools = data_tools
        self.check_tools = check_tools
        # Add logging entry
        self.server_logger.warning('{!s} Received at {!s}'.format(self.logger_sentence,
                                                                  config.get('request_timestamp')))

    def train(self, train_data):
        # Create trained model directory
        result_path = '{!s}/{!s}/{!s}/res'.format(self.model_path, self.train_scenario_id, self.substation_id)
        if not os.path.isdir(os.path.dirname(result_path)):
            os.makedirs(result_path)
        # Import python model handle.
        model_handle_path = self.model_path + '/model_handle'
        model_handle = importlib.import_module(model_handle_path.replace('/', '.'), package=None)
        # Create config file for the training.
        model_config = {'model_id': self.model_id, 'train_scenario': self.train_scenario_id,
                        'substation_id': self.substation_id, 'model_path': self.model_path,
                        'model_info': self.model_info}

        # Run preprocessing steps
        cropped_data = self.data_tools.crop_data(train_data)
        filtered_data = self.data_tools.filter_data(cropped_data, self.model_info.get('filter_config'))
        resampled_data = self.data_tools.resample_data(filtered_data, self.model_info.get('resample_config'))

        # Pass resampled data and model config to model handle for xy_creation and train evaluation.
        model_handle = model_handle.Model_Handle(model_config, self.matlabeng)
        xy_data = model_handle.xy_creation(resampled_data, True)

        # Check if xy_data are valid (currently just checks if they're nonzero - Feature added for future expansion)
        xy_data_check = CheckTools.xy_data_check(xy_data)
        if xy_data_check == 'xy_data_valid':
            return_flag = model_handle.run_train(xy_data)
            self.server_logger.warning('{!s} {!s}'.format(self.logger_sentence, return_flag))
        else:
            # Delete newly created directory.
            return_flag = 'xy_data_invalid'
            rmtree(result_path)
            self.server_logger.warning('{!s} {!s}'.format(self.logger_sentence, xy_data_check))
        return return_flag

    def preprocessed_data(self, data):
        """
        NOTE: this is only for the /preprocessed_data feature of the server. It's not intended to be used anywhere else.
        """
        # Import model_handle. Create config file. Pass it, along with input data.
        model_handle_path = self.model_path + '/model_handle'
        model_handle = importlib.import_module(model_handle_path.replace('/', '.'), package=None)
        model_config = {'model_id': self.model_id, 'train_scenario': self.train_scenario_id,
                        'substation_id': self.substation_id, 'model_path': self.model_path}

        # Run preprocessing steps
        cropped_data = self.data_tools.crop_data(data)
        filtered_data = self.data_tools.filter_data(cropped_data, self.model_info.get('filter_config'))
        resampled_data = self.data_tools.resample_data(filtered_data, self.model_info.get('resample_config'))

        # Pass resampled data and model config to model handle for xy_creation and train evaluation.
        model_handle = model_handle.Model_Handle(model_config, self.matlabeng)
        xy_data = model_handle.xy_creation(resampled_data, True)

        # Check if xy_data are valid (currently just checks if they're nonzero - Feature added for future expansion)
        xy_data_check = CheckTools.xy_data_check(xy_data)
        if xy_data_check == 'xy_data_valid':
            self.server_logger.warning('{!s}  complete'.format(self.logger_sentence))
            # Modify the results from Dataframe to json
            output = xy_data
        else:
            self.server_logger.warning('{!s}  Zero XY Data'.format(self.logger_sentence))
            output = 'preprocess_fail_xy_data_zero'
        return output

    def update_yaml(self):
        mf = pathlib.Path('paths.yaml')
        with open(mf) as fp:
            yaml_dict = yaml.safe_load(fp)
        trained_model_dict = yaml_dict.get('trained_model_paths').get(self.model_id)
        path_to_assign = '{!s}/{!s}/{!s}'.format(self.model_path, self.train_scenario_id, self.substation_id)
        if self.train_scenario_id not in trained_model_dict:
            trained_model_dict[self.train_scenario_id] = {self.substation_id: path_to_assign}
        else:
            trained_model_dict.get(self.train_scenario_id)[self.substation_id] = path_to_assign

        with open('paths.yaml', 'w') as fp:
            yaml.safe_dump(yaml_dict, fp)
        output_msg = "train_complete__path_file_updated"
        self.server_logger.warning('{!s} {!s}'.format(self.logger_sentence, output_msg))
        return output_msg
