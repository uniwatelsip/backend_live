import pandas as pd
import datetime as dt
import numpy as np
import matlab.engine
from check_tools import CheckTools


class DataTools:

    def __init__(self, data_config, mateng, logger, flag):

        self.timestamp = data_config.get('request_timestamp')
        self.data_config = data_config
        self.server_logger = logger
        self.logger_sentence = self.data_config.get('logger_sentence')
        self.mateng = mateng
        self.model_info = self.data_config['model_infos'].get(self.data_config['model_id'])
        self.flag = flag

    def merge_data(self, dataset):
        check_tools = CheckTools(self.data_config, self.server_logger)

        # This if discerns between direct_eval and regular train/eval - in a direct_eval process, a dataset would be
        # supplied. Otherwise, it has to be loaded from the database using the data_config information.
        if dataset is None:
            dataset_path = self.data_config['dataset_paths'].get(self.data_config['substation_id'])
            dataset_path = '{!s}.csv'.format(dataset_path)
            try:
                dataset = pd.read_csv(dataset_path, parse_dates=['SL_DateTime'])
            except FileNotFoundError:
                error_msg = 'internal_error: dataset_not_found'
                self.server_logger.exception('{!s}  {!s}'.format(self.logger_sentence, error_msg))
                return error_msg
        else:
            dataset['SL_DateTime'] = pd.to_datetime(dataset['SL_DateTime'])   # Making sure dates are parsed correctly.
        dataset.set_index(pd.to_datetime(dataset['SL_DateTime']), inplace=True)

        # This if creates the scenario column for train, eval, or direct_eval processes.
        if self.flag == 'Eval':
            # Load test scenario from database
            test_scenario_s = 's' + self.data_config['test_scenario'][1:]
            scenario_path = self.data_config['scenario_paths'].get(test_scenario_s)
            scenario_path = '{!s}.csv'.format(scenario_path)
            try:
                scenario = pd.read_csv(scenario_path, parse_dates=['SL_DateTime'])
            except FileNotFoundError:
                error_msg = 'internal_error: scenario_not_found'
                self.server_logger.exception('{!s}  {!s}'.format(self.logger_sentence, error_msg))
                return error_msg

        elif self.flag == 'Train':
            # Load train scenario from database
            train_scenario_s = 's' + self.data_config['train_scenario'][2:]
            scenario_path = self.data_config['scenario_paths'].get(train_scenario_s)
            scenario_path = '{!s}.csv'.format(scenario_path)
            try:
                scenario = pd.read_csv(scenario_path, parse_dates=['SL_DateTime'])
            except FileNotFoundError:
                error_msg = 'internal_error: scenario_not_found'
                self.server_logger.exception('{!s}  {!s}'.format(self.logger_sentence, error_msg))
                return error_msg

        else:   # flag == 'direct_eval'
            # Create dummy scenario with True values everywhere, for direct evaluation of trained model.
            dataset.set_index(dataset['SL_DateTime'], inplace=True)
            first_datetime = dataset.index[0]
            last_datetime = dataset.index[-1]
            scenario_index = pd.date_range(first_datetime, last_datetime, freq=pd.Timedelta('1min'))
            scenario = pd.DataFrame(True, index=scenario_index, columns=['Filter'])
            scenario['SL_DateTime'] = scenario_index.values

        scenario.set_index(pd.to_datetime(scenario['SL_DateTime']), inplace=True)

        # First, verify that the filter datetimes do not exceed the dataset bounds. Then merge the datasets.
        check_datetimes = check_tools.filter_datetime_check(scenario, dataset)
        if check_datetimes == "Supplied_filter_within_datetime_bounds":
            new_df = dataset.merge(scenario['Filter'].to_frame(), left_index=True, right_index=True, how='outer')
            new_df.sort_index(inplace=True)
            new_df['Filter'] = new_df['Filter'].fillna(method='ffill', limit=10)
            # Drop rows where SL_DateTime is NaN.
            new_df = new_df[pd.notnull(new_df['SL_DateTime'])]
            msg = 'Dataset_merging_successful'
            self.server_logger.warning('{!s}  {!s}'.format(self.logger_sentence, msg))
        else:
            # Merging unsuccessful.
            msg = check_datetimes
            return msg

        # Check NaN percent threshold of merged dataset. Note: for model training, this is a priori specified
        # by the model creator. For model evaluation (either eval or direct_eval), this is set to 0.9.
        if self.flag == 'Train':
            active_columns = self.model_info.get('inputs')
            nan_threshold = self.model_info.get('nan_thres')
            check_nans = check_tools.nan_threshold_check(new_df, active_columns, nan_threshold)
        else:
            active_columns = self.model_info.get('inputs')
            nan_threshold = 0.9
            check_nans = check_tools.nan_threshold_check(new_df, active_columns, nan_threshold)

        if check_nans == "nan_percent_below_threshold":
            output = new_df
        else:
            output = check_nans

        return output

    def crop_data(self, raw_data):
        # This function crops the dataset in an optimal way, so that unnecessary operations are avoided.

        # Find first and last occurrence of True values in the filter column.
        first_filter_date = raw_data.loc[raw_data['Filter'] == True].index[0]
        last_filter_date = raw_data.loc[raw_data['Filter'] == True].index[-1]
        model_horizon = self.model_info.get('reg_horizon_hr')       # Get the model regressive horizon in hrs.
        model_horizon = '{!s}hr'.format(str(model_horizon))
        # Crop data based on the regressive horizon of the model ( = the maximum length of time of which past data are
        # required in order to create the inputs). Log a warning in case this exceeds the start date of the dataset.
        crop_date = first_filter_date-pd.Timedelta(model_horizon)
        if (crop_date < raw_data.index[0]) & (self.flag != 'direct_eval'):
            self.server_logger.warning('{!s}  !WARNING! Crop date exceeds '
                                       'dataset start date'.format(self.logger_sentence))
        cropped_data = raw_data[crop_date:last_filter_date]
        cropped_data.sort_index(inplace=True)
        return cropped_data

    def filter_data(self, cropped_data, filter_config):
        # Filter out datapoints with non-applicable datetimes
        data_length = cropped_data.shape[0]
        datetime_start = dt.datetime.strptime('2013-01-01 00:00:00', '%Y-%m-%d %H:%M:%S')
        datetime_end = dt.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
        faulty_AP = (cropped_data['SL_Active'] < -50) | (cropped_data['SL_Active'] > 50)
        faulty_datetimes = (cropped_data.index < datetime_start) | (cropped_data.index > datetime_end)
        filtered_values = faulty_AP.sum() + faulty_datetimes.sum()    # counter for bad values (used throughout the fnc)

        # Drop bad values
        cropped_data.mask(faulty_AP, np.nan, inplace=True)
        cropped_data = cropped_data.drop(cropped_data[faulty_datetimes].index, axis=1)

        # Remove duplicate indices (if any)
        cropped_data = cropped_data.loc[~cropped_data.index.duplicated(keep='first')]
        cropped_data.dropna(subset=['SL_DateTime'], inplace=True)
        cropped_data.sort_index(inplace=True)

        # Outlier detection and Rejection:
        # Prepare data for matlab API
        matlab_ap_vector = matlab.double(cropped_data['SL_Active'].values.tolist())
        matlab_datetime_vector = pd.to_timedelta(cropped_data['SL_DateTime']).dt.total_seconds()
        matlab_datetime_vector = matlab.double(matlab_datetime_vector.values.tolist())
        matlabengine = self.mateng

        # Check data for outliers using outlierdetect.m:
        try:
            is_outlier = matlabengine.outlierdetect(matlab_ap_vector, matlab_datetime_vector,
                                                    filter_config.get('od_method'), filter_config.get('mov_window'),
                                                    filter_config.get('threshold_factor'))
        except RuntimeError:
            error_msg = 'matlab_outlier_detect_error'
            self.server_logger.exception('{!s} {!s}'.format(self.timestamp, error_msg))
            output = error_msg
            return output
        is_outlier = np.asarray(is_outlier._data)                        # Convert to array
        outlier_filter = pd.Series(is_outlier, index=cropped_data.index)     # Create outlier mask (1 = outlier)
        filtered_values = filtered_values + outlier_filter.sum()
        outlier_filter = outlier_filter == 1
        cropped_data[outlier_filter] = np.nan                                # Reject outliers

        # Zero variance data detection & rejection
        # Zero Variance data occur when, while there is a loss of voltage at the transformer, the active power sensor
        # continues to log data. The result is a signal resembling gaussian white noise with a mean value around 0-0.5
        # MW and a std of 0.04-0.07 sqrt(MW).
        # This part of the filtering routine attempts to identify and dispose those sections of data.
        ap_data_std = cropped_data['SL_Active'].rolling(filter_config['std_window']).std()
        ap_data_mean = cropped_data['SL_Active'].rolling(filter_config['mean_window']).mean()
        std_filter = ap_data_std < filter_config['std_threshold']
        mean_filter = (abs(ap_data_mean) < filter_config['mean_upper_threshold']) & \
                      (abs(ap_data_mean) > filter_config['mean_lower_threshold'])
        zvd_filter = std_filter & mean_filter
        filtered_values = filtered_values + sum(zvd_filter)
        cropped_data[zvd_filter] = np.nan       # Reject ZVD data

        filtered_values_percent = round(filtered_values/data_length*100)
        self.server_logger.warning('{!s} {!s} datapoints filtered out( {!s} % )'.format(self.logger_sentence,
                                                                                        filtered_values,
                                                                                        filtered_values_percent))
        filtered_data = cropped_data
        return filtered_data

    @staticmethod
    def static_resample(data, resample_config):
        resamp_freq = resample_config['frequency']
        nan_threshold = resample_config['nan_resamp_threshold']
        active_cols = resample_config['active_columns']
        filter_col = data.pop('Filter')
        filter_col = filter_col == True
        data = data[active_cols]
        data['Filter'] = filter_col

        resamp_aggr = data.resample(resamp_freq, loffset=resamp_freq, closed='right').agg(['mean', 'count'])
        filter_column = filter_col.resample(resamp_freq).fillna('backfill', limit=10)
        invalid = resamp_aggr['SL_Active']['count'] <= nan_threshold
        # Render the invalid measurements to NaNs
        resamp_aggr[invalid] = np.nan
        # This if robustifies the resample process: if the preprocess algorithm comes across
        # data with problematic sampling rates (as in, greater than 10', compared to the usual 1'),
        # then upsample original data to 1'
        if resamp_aggr['SL_Active']['mean'].dropna().shape[0] == 0:
            data = data.resample('1min').interpolate(method='linear')
            resamp_aggr = data.resample(resamp_freq, loffset=resamp_freq, closed='right').agg(['mean', 'count'])
            invalid = resamp_aggr['SL_Active']['count'] <= 2
            # Render the invalid measurements to NaNs
            resamp_aggr[invalid] = np.nan

        data_resamp = pd.DataFrame(index=resamp_aggr.index, columns=active_cols)
        # Grab features
        for i in range(len(active_cols)):
            feature = resamp_aggr[active_cols[i]]['mean']
            data_resamp[active_cols[i]] = feature
        data_resamp['Filter'] = filter_column

        return data_resamp

    @staticmethod
    def rolling_resample(data, resample_config):
        resamp_freq = resample_config['frequency']
        nan_threshold = resample_config['nan_resamp_threshold']
        active_cols = resample_config['active_columns']
        filter_col = data.pop('Filter')
        filter_col = filter_col == True
        data = data[active_cols]
        data['Filter'] = filter_col

        # Resample the data on minute timestamps
        filter_col = data.pop('Filter')  # Pop filter column (its boolean, can't be interpolated, lol)
        fixed_freq_range = pd.date_range(start=data.index[0].floor('min'), end=data.index[-1].floor('min'), freq='1min')
        data_ff = data.reindex(data.index | fixed_freq_range).interpolate(method='index',
                                                                          limit_direction='both',
                                                                          limit=10).loc[fixed_freq_range]

        # Reappend the filter column
        filter_col = filter_col.reindex(data_ff.index, method='nearest')
        data_ff['Filter'] = filter_col
        # Resample the active power data with a rolling window.
        data_resamp = data_ff.rolling(resamp_freq, min_periods=nan_threshold).mean()
        # Round Filter column because the averaging operation of resampling would leave some values as non-integer.
        data_resamp['Filter'] = data_resamp['Filter'].round(decimals=0)
        return data_resamp

    def resample_data(self, filtered_data, resample_config):
        if resample_config.get('type') == 'static':
            resampled_data = self.static_resample(filtered_data, resample_config)
        else:
            resampled_data = self.rolling_resample(filtered_data, resample_config)
        return resampled_data
