import pandas as pd
from telsipbackend import TelsipBackend
import matlab.engine
mateng = matlab.engine.start_matlab()
import threading
import logging
from datetime import datetime

lock = threading.Lock()
# start logger
# Create logger
server_logger = logging.getLogger('server_logger')
server_timestamp = datetime.now().strftime('%Y-%m-%d_%H_%M_%S')
filename_string = 'serverlog.log'
# Create handlers
c_handler = logging.StreamHandler()
f_handler = logging.FileHandler('server.log')
c_handler.setLevel(logging.INFO)
f_handler.setLevel(logging.WARNING)
# Add handlers to the logger
server_logger.addHandler(c_handler)
server_logger.addHandler(f_handler)
# log server timestamp
server_logger.warning('Started Server @ {!s}'.format(server_timestamp))
# end logger
#
# od_config = {'od_method': 'movmedian', 'mov_window': 60, 'threshold_factor': 2.2,
#              'substation_id': "st00003", 'date_from': "2016-01-01 00:00:00", 'date_to': "2016-01-05 00:00:00"}
# backend = TelsipBackend(od_config, mateng, lock, server_logger)
# # Run eval
# output = backend.outlier_detection(od_config)
# print(output)
# ===Train model mt004e===
# config = {'model_id': 'mt010', 'train_scenario': 'sc013_02', 'substation_id': 'st00046', 'test_scenario': 't013_03',
#           'from': '2017-05-27 00:00:00', 'to': '2017-05-28 00:00:00', 'overwrite_model': True, 'overwrite_run': True}
# backend = TelsipBackend(config, mateng, lock, server_logger)
# backend.train()
# backend.eval()

# config = {'model_id': 'mt001', 'train_scenario': 'sc035_02', 'substation_id': 'st00046', 'test_scenario': 't035_03',
#           'from': '2017-05-27 00:00:00', 'to': '2017-05-28 00:00:00', 'overwrite_model': False, 'overwrite_run': True}
# backend = TelsipBackend(config, mateng, lock, server_logger)
# backend.train()
# backend.eval()

# config = {'model_id': 'mt001', 'train_scenario': 'sc013_02', 'substation_id': 'st00046', 'test_scenario': 't013_02',
#           'from': '2017-05-27 00:00:00', 'to': '2017-05-28 00:00:00', 'overwrite_model': True, 'overwrite_run': True}
# backend = TelsipBackend(config, mateng, lock, server_logger)
# backend.eval()

config = {'model_id': 'mt001', 'train_scenario': 'sc035_02', 'substation_id': 'st0006', 'preprocess_data': True}
backend = TelsipBackend(config, mateng, lock, server_logger)
backend.data_grab()