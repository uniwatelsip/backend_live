import numpy as np
import pandas as pd
import datetime as dt

class Preprocessor:
    def __init__(self, raw_data):

        # Find first and last occurence of True values in the filter column. Keep only values inbetween, in order to
        # avoid unnecessary string-to-datetime conversion @ datetime column (slow process).

        first_filter_loc = raw_data.loc[raw_data['Filter'] == True].index[0]
        last_filter_loc = raw_data.loc[raw_data['Filter'] == True].index[-1]
        raw_data_filter_range = raw_data[first_filter_loc-pd.Timedelta('14d'):last_filter_loc]   # grab 2 weeks extra.
        #data_index = raw_data_filter_range['SL_DateTime']
        #data_index = pd.to_datetime(data_index, format='%Y-%m-%d %H:%M:%S')
        #raw_data_filter_range.set_index(data_index, inplace=True)                    # Set datetime index
        raw_data_filter_range.sort_index(inplace=True)                               # Sort datetime index
        self.input_data = raw_data_filter_range

    def filtering(self):

        # Filter out the following:
        # 1) Datetimes outside the range of 2013-01-01 and todays date
        # 2) AP outside the range of [50,50]

        data = self.input_data
        datetime_start = dt.datetime.strptime('2013-01-01 00:00:00', '%Y-%m-%d %H:%M:%S')
        datetime_end = dt.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
        faulty_AP = (self.input_data['SL_Active'] < -50) | (self.input_data['SL_Active'] > 50)
        faulty_datetimes = (self.input_data.index < datetime_start) | (self.input_data.index > datetime_end)

        # Drop bad values
        data.mask(faulty_AP, np.nan, inplace=True)
        data = data.drop(data[faulty_datetimes].index, axis=1)
        # Remove duplicate indices (if any)
        data = data.loc[~data.index.duplicated(keep='first')]
        data.dropna(subset=['SL_DateTime'], inplace=True)
        data.dropna(subset=['SL_Active'], inplace=True)
        data.sort_index(inplace=True)
        self.input_data = data

    def resampling(self):
        data = self.input_data[['SL_Active', 'SL_Reactive', 'SL_Voltage', 'SL_ir3_Tap', 'Wt_Clouds', 'Wt_Speed',
                                'Wt_Temp', 'Filter']]
        # Make sure Filter column is boolean
        filter_col = data.pop('Filter')
        filter_col = filter_col == True
        data['Filter'] = filter_col
        # Resample the data on minute timestamps
        filter_col = data.pop('Filter')             # Pop filter column (its boolean, can't be interpolated, lol)
        fixed_freq_range = pd.date_range(start=data.index[0].floor('min'), end=data.index[-1].floor('min'), freq='1min')
        data_ff = data.reindex(data.index | fixed_freq_range).interpolate(method='index', limit_direction='both', limit=2).loc[fixed_freq_range]

        # Reappend the filter column
        filter_col = filter_col.reindex(data_ff.index, method='nearest')
        data_ff['Filter'] = filter_col
        # Resample the active power data with a rolling window.
        resampled_rolling = data_ff.rolling('15min', min_periods=8).mean()
        # Grab the columns and create a new dataframe, avg'd @ 15mins.
        data_resamp = pd.DataFrame(
            {'SL_Active': resampled_rolling['SL_Active'],
             'hd_clouds': resampled_rolling['Wt_Clouds'],
             'hd_temperature': resampled_rolling['Wt_Temp'],
             'hd_wind_speed': resampled_rolling['Wt_Speed'],
             'Filter': resampled_rolling['Filter']
             })
        # Round Filter column because the averaging operation of resampling would leave some values as non-integer.
        data_resamp['Filter'] = data_resamp['Filter'].round(decimals=0)
        self.input_data = data_resamp


    def xy_creation(self):
        timestep = pd.Timedelta('1h')  # Model Timestep

        ap_data = self.input_data['SL_Active']
        cloud_data = self.input_data['Wt_Clouds']
        temp_data = self.input_data['Wt_Temp']
        wind_data = self.input_data['Wt_Speed']
        filter_column = self.input_data['Filter'] == 1

        # XY inputs creation
        out_ap_val_next_1hr = ap_data.shift(periods=-1, freq=timestep)  # Model Output

        # ---- Active Power Inputs -----
        # Filter out ap values that correspond to cloud weather
        # cloudy_filter = cloud_data >= 20
        # ap_data[cloudy_filter] = np.nan
        # create ap inputs
        in_ap_val_current = ap_data
        in_ap_val_prev_15min = ap_data.shift(periods=1, freq=pd.Timedelta('15min'))
        in_ap_val_prev_30min = ap_data.shift(periods=2, freq=pd.Timedelta('15min'))
        in_ap_val_prev_45min = ap_data.shift(periods=3, freq=pd.Timedelta('15min'))
        in_ap_val_prev_1hr = ap_data.shift(periods=1, freq=timestep)
        in_ap_val_prev_2hr = ap_data.shift(periods=2, freq=timestep)
        in_ap_avg_last_hr = ap_data.rolling(window='1h').mean()
        in_ap_avg_last_2hr = ap_data.rolling(window='2h').mean()
        in_ap_avg_last_3hr = ap_data.rolling(window='3h').mean()
        in_ap_avg_last_4hr = ap_data.rolling(window='4h').mean()

        in_ap_val_next_1hr_prev_day = ap_data.shift(periods=1, freq=pd.Timedelta('1d') - timestep)
        in_ap_val_current_prev_day = ap_data.shift(periods=1, freq=pd.Timedelta('1d'))
        in_ap_avg_last_1hr_prev_day = ap_data.shift(periods=1, freq=pd.Timedelta('1d')).rolling(window='1h').mean()
        in_ap_val_next_1hr_prev_week = ap_data.shift(periods=1, freq=pd.Timedelta('7d') - timestep)
        in_ap_val_current_prev_week = ap_data.shift(periods=1, freq=pd.Timedelta('7d'))
        in_ap_avg_last_1hr_prev_week = ap_data.shift(periods=1, freq=pd.Timedelta('7d')).rolling(window='1h').mean()

        in_ap_delta_prev_1hr_current = ap_data - ap_data.shift(periods=1, freq=timestep)
        in_ap_delta_prev_2hr_current = ap_data - ap_data.shift(periods=2, freq=timestep)
        in_ap_delta_prev_3hr_current = ap_data - ap_data.shift(periods=2, freq=timestep)

        # ---- Weather Inputs -----
        in_clds_val_current = cloud_data
        in_clds_val_next_1hr = cloud_data.shift(periods=-1, freq=timestep)
        in_clds_val_next_2hr = cloud_data.shift(periods=-2, freq=timestep)
        in_clds_delta_next_1hr_current = cloud_data - cloud_data.shift(periods=-1, freq=timestep)
        in_clds_delta_next_3hr_current = cloud_data - cloud_data.shift(periods=-3, freq=timestep)

        in_wspd_val_current = wind_data
        in_wspd_val_next_1hr = wind_data.shift(periods=-1, freq=timestep)
        in_wspd_val_next_2hr = wind_data.shift(periods=-2, freq=timestep)
        in_wspd_delta_next_1hr_current = wind_data - wind_data.shift(periods=-1, freq=timestep)
        in_wspd_delta_next_3hr_current = wind_data - wind_data.shift(periods=-3, freq=timestep)

        in_temp_val_current = temp_data
        in_temp_val_next_1hr = temp_data.shift(periods=-1, freq=timestep)
        in_temp_val_next_2hr = temp_data.shift(periods=-2, freq=timestep)
        in_temp_delta_next_1hr_current = temp_data - temp_data.shift(periods=-1, freq=timestep)
        in_temp_delta_next_3hr_current = temp_data - temp_data.shift(periods=-3, freq=timestep)


        dates = pd.Series(self.input_data.index.values, index=self.input_data.index)
        # Create XY data table
        xy_data_all = pd.concat([dates, in_ap_val_current, in_ap_val_prev_15min, in_ap_val_prev_30min,
                                in_ap_val_prev_45min,
                                in_ap_val_prev_1hr, in_ap_val_prev_2hr, in_ap_avg_last_hr,
                                in_ap_val_next_1hr_prev_day, in_ap_avg_last_1hr_prev_day,
                                in_ap_val_next_1hr_prev_week, in_ap_avg_last_1hr_prev_week,
                                in_ap_avg_last_2hr, in_ap_avg_last_3hr,
                                in_ap_avg_last_4hr, in_ap_delta_prev_1hr_current,
                                in_ap_delta_prev_2hr_current, in_ap_delta_prev_3hr_current,
                                in_ap_val_current_prev_day, in_ap_val_current_prev_week,
                                in_clds_val_current, in_clds_val_next_1hr, in_clds_val_next_2hr,
                                in_clds_delta_next_1hr_current, in_clds_delta_next_3hr_current, in_wspd_delta_next_3hr_current,
                                in_wspd_val_current, in_wspd_val_next_1hr, in_wspd_val_next_2hr, in_wspd_delta_next_1hr_current,
                                in_temp_val_current, in_temp_val_next_1hr, in_temp_val_next_2hr,
                                in_temp_delta_next_1hr_current,
                                in_temp_delta_next_3hr_current, out_ap_val_next_1hr],
                                axis=1)
        xy_data_all.columns = ['datetime', 'in_ap_val_current', 'in_ap_val_prev_15min', 'in_ap_val_prev_30min',
                                'in_ap_val_prev_45min',
                                'in_ap_val_prev_1hr', 'in_ap_val_prev_2hr', 'in_ap_avg_last_hr',
                                'in_ap_val_next_1hr_prev_day', 'in_ap_avg_last_1hr_prev_day',
                                'in_ap_val_next_1hr_prev_week', 'in_ap_avg_last_1hr_prev_week',
                                'in_ap_avg_last_2hr', 'in_ap_avg_last_3hr',
                                'in_ap_avg_last_4hr', 'in_ap_delta_prev_1hr_current',
                                'in_ap_delta_prev_2hr_current', 'in_ap_delta_prev_3hr_current',
                                'in_ap_val_current_prev_day', 'in_ap_val_current_prev_week',
                                'in_clds_val_current', 'in_clds_val_next_1hr', 'in_clds_val_next_2hr',
                                'in_clds_delta_next_1hr_current', 'in_clds_delta_next_3hr_current', 'in_wspd_delta_next_3hr_current',
                                'in_wspd_val_current', 'in_wspd_val_next_1hr', 'in_wspd_val_next_2hr', 'in_wspd_delta_next_1hr_current',
                                'in_temp_val_current', 'in_temp_val_next_1hr', 'in_temp_val_next_2hr',
                                'in_temp_delta_next_1hr_current',
                                'in_temp_delta_next_3hr_current', 'target']

        # Filter out the desired datetimes based on scenario preprocessing and pass back the xy_data.

        cut_bad_dates = (xy_data_all.index >= filter_column.index.values[0]) & \
                        (xy_data_all.index <= filter_column.index.values[-1])
        xy_data_all = xy_data_all[cut_bad_dates]
        xy_data_all = xy_data_all[filter_column]
        print(xy_data_all.describe())
        self.xy_data = xy_data_all
        return self.xy_data
