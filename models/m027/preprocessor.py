import numpy as np
import pandas as pd
import datetime as dt

class Preprocessor:
    def __init__(self, raw_data):

        # Find first and last occurence of True values in the filter column. Keep only values inbetween, in order to
        # avoid unnecessary string-to-datetime conversion @ datetime column (slow process).

        first_filter_loc = raw_data.loc[raw_data['Filter'] == True].index[0]
        last_filter_loc = raw_data.loc[raw_data['Filter'] == True].index[-1]
        raw_data_filter_range = raw_data[first_filter_loc-pd.Timedelta('14d'):last_filter_loc]   # grab 2 weeks extra.
        #data_index = raw_data_filter_range['SL_DateTime']
        #data_index = pd.to_datetime(data_index, format='%Y-%m-%d %H:%M:%S')
        #raw_data_filter_range.set_index(data_index, inplace=True)                    # Set datetime index
        raw_data_filter_range.sort_index(inplace=True)                               # Sort datetime index
        self.input_data = raw_data_filter_range

    def filtering(self):

        # Filter out the following:
        # 1) Datetimes outside the range of 2013-01-01 and todays date
        # 2) AP outside the range of [50,50]

        data = self.input_data
        datetime_start = dt.datetime.strptime('2013-01-01 00:00:00', '%Y-%m-%d %H:%M:%S')
        datetime_end = dt.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
        faulty_AP = (self.input_data['SL_Active'] < -50) | (self.input_data['SL_Active'] > 50)
        faulty_datetimes = (self.input_data.index < datetime_start) | (self.input_data.index > datetime_end)

        # Drop bad values
        data.mask(faulty_AP, np.nan, inplace=True)
        data = data.drop(data[faulty_datetimes].index, axis=1)
        # Remove duplicate indices (if any)
        data = data.loc[~data.index.duplicated(keep='first')]
        data.dropna(subset=['SL_DateTime'], inplace=True)
        data.sort_index(inplace=True)
        self.input_data = data

    def resampling(self):
        data = self.input_data[['SL_Active', 'SL_Reactive', 'SL_Voltage', 'SL_ir3_Tap', 'Wt_Clouds', 'Wt_Speed',
                                'Wt_Temp', 'Filter']]
        # Make sure Filter column is boolean
        filter_col = data.pop('Filter')
        filter_col = filter_col == True
        data['Filter'] = filter_col

        resamp_aggr = data.resample('1h', loffset='1h', closed='right').agg(['mean', 'count'])
        invalid = resamp_aggr['SL_Active']['count'] <= 30
        # Render the invalid measurements to NaNs
        resamp_aggr[invalid] = np.nan
        # Grab the columns and create a new dataframe, avg'd @ 15mins.
        data_resamp = pd.DataFrame(
            {'SL_Active': resamp_aggr['SL_Active']['mean'],
             'hd_clouds': resamp_aggr['Wt_Clouds']['mean'],
             'hd_temperature': resamp_aggr['Wt_Temp']['mean'],
             'hd_wind_speed': resamp_aggr['Wt_Speed']['mean'],
             'Filter': resamp_aggr['Filter']['mean']
             })
        # Round Filter column because the averaging operation of resampling would leave some values as non-integer.
        data_resamp['Filter'] = data_resamp['Filter'].round(decimals=0)
        self.input_data = data_resamp


    def xy_creation(self):
        timestep = pd.Timedelta('24h')  # Model Timestep

        ap_data = self.input_data['SL_Active']
        cloud_data = self.input_data['Wt_Clouds']
        temp_data = self.input_data['Wt_Temp']
        wind_data = self.input_data['Wt_Speed']
        rhumid_data = self.input_data['Wt_RHumid']
        filter_column = self.input_data['Filter'] == 1

        # XY inputs creation
        out_ap_val_next_24hr = ap_data.shift(periods=-1, freq=timestep)  # Model Output

        # ---- Active Power Inputs -----
        # Filter out ap values that correspond to cloud weather
        # cloudy_filter = cloud_data >= 20
        # ap_data[cloudy_filter] = np.nan
        # create ap inputs
        in_ap_val_current = ap_data
        in_ap_val_prev_1hr = ap_data.shift(periods=1, freq=pd.Timedelta('1hr'))
        in_ap_val_prev_2hr = ap_data.shift(periods=2, freq=pd.Timedelta('1hr'))
        in_ap_val_prev_3hr = ap_data.shift(periods=3, freq=pd.Timedelta('1hr'))
        in_ap_val_prev_4hr = ap_data.shift(periods=4, freq=pd.Timedelta('1hr'))
        in_ap_val_prev_22hr = ap_data.shift(periods=22, freq=pd.Timedelta('1hr'))
        in_ap_val_prev_23hr = ap_data.shift(periods=23, freq=pd.Timedelta('1hr'))
        in_ap_val_prev_24hr = ap_data.shift(periods=24, freq=pd.Timedelta('1hr'))
        in_ap_val_prev_25hr = ap_data.shift(periods=25, freq=pd.Timedelta('1hr'))
        in_ap_val_prev_26hr = ap_data.shift(periods=26, freq=pd.Timedelta('1hr'))

        in_ap_avg_last_hr = ap_data.rolling(window='1h').mean()
        in_ap_avg_last_2hr = ap_data.rolling(window='2h').mean()
        in_ap_avg_last_3hr = ap_data.rolling(window='3h').mean()

        in_ap_avg_last_3hr_prev_day = ap_data.shift(periods=1, freq=pd.Timedelta('1d')).rolling(window='3h').mean()
        in_ap_val_next_22hr_prev_week = ap_data.shift(periods=1, freq=pd.Timedelta('7d') - pd.Timedelta('22hr'))
        in_ap_val_next_23hr_prev_week = ap_data.shift(periods=1, freq=pd.Timedelta('7d') - pd.Timedelta('23hr'))
        in_ap_val_next_24hr_prev_week = ap_data.shift(periods=1, freq=pd.Timedelta('7d') - pd.Timedelta('24hr'))
        in_ap_val_next_25hr_prev_week = ap_data.shift(periods=1, freq=pd.Timedelta('7d') - pd.Timedelta('25hr'))
        in_ap_val_next_26hr_prev_week = ap_data.shift(periods=1, freq=pd.Timedelta('7d') - pd.Timedelta('26hr'))
        in_ap_val_current_prev_week = ap_data.shift(periods=1, freq=pd.Timedelta('7d'))
        in_ap_avg_last_3hr_prev_week = ap_data.shift(periods=1, freq=pd.Timedelta('7d')).rolling(window='3h').mean()

        in_ap_delta_prev_1hr_current = ap_data - ap_data.shift(periods=1, freq=pd.Timedelta('1hr'))
        in_ap_delta_prev_2hr_current = ap_data - ap_data.shift(periods=2, freq=pd.Timedelta('1hr'))
        in_ap_delta_prev_3hr_current = ap_data - ap_data.shift(periods=3, freq=pd.Timedelta('1hr'))

        in_ap_delta_prev_23hr_prev_24hr = ap_data.shift(periods=1, freq=pd.Timedelta('23hr')) - \
                                          ap_data.shift(periods=1, freq=pd.Timedelta('24hr'))
        in_ap_delta_prev_24hr_prev_25hr = ap_data.shift(periods=1, freq=pd.Timedelta('24hr')) - \
                                          ap_data.shift(periods=1, freq=pd.Timedelta('25hr'))
        in_ap_delta_prev_25hr_prev_26hr = ap_data.shift(periods=1, freq=pd.Timedelta('25hr')) - \
                                          ap_data.shift(periods=1, freq=pd.Timedelta('26hr'))

        # ---- Weather Inputs -----
        in_clds_val_current = cloud_data
        in_clds_val_next_23hr = cloud_data.shift(periods=-23, freq=pd.Timedelta('1hr'))
        in_clds_val_next_24hr = cloud_data.shift(periods=-24, freq=pd.Timedelta('1hr'))
        in_clds_val_next_25hr = cloud_data.shift(periods=-25, freq=pd.Timedelta('1hr'))
        in_clds_avg_last_24hr_next_day = cloud_data.shift(periods=-1, freq=pd.Timedelta('24h')).rolling(window='24h').mean()

        in_wspd_val_current = wind_data
        in_wspd_val_next_23hr = wind_data.shift(periods=-23, freq=pd.Timedelta('1hr'))
        in_wspd_val_next_24hr = wind_data.shift(periods=-24, freq=pd.Timedelta('1hr'))
        in_wspd_val_next_25hr = wind_data.shift(periods=-25, freq=pd.Timedelta('1hr'))
        in_wspd_avg_last_24hr_next_day = wind_data.shift(periods=-1, freq=pd.Timedelta('24h')).rolling(window='24h').mean()

        in_temp_val_current = temp_data
        in_temp_val_next_23hr = temp_data.shift(periods=-23, freq=pd.Timedelta('1hr'))
        in_temp_val_next_24hr = temp_data.shift(periods=-24, freq=pd.Timedelta('1hr'))
        in_temp_val_next_25hr = temp_data.shift(periods=-25, freq=pd.Timedelta('1hr'))
        in_temp_avg_last_24hr_next_day = temp_data.shift(periods=-1, freq=pd.Timedelta('24h')).rolling(window='24h').mean()

        in_rhumid_val_current = rhumid_data
        in_rhumid_val_next_23hr = rhumid_data.shift(periods=-23, freq=pd.Timedelta('1hr'))
        in_rhumid_val_next_24hr = rhumid_data.shift(periods=-24, freq=pd.Timedelta('1hr'))
        in_rhumid_val_next_25hr = rhumid_data.shift(periods=-25, freq=pd.Timedelta('1hr'))
        in_rhumid_avg_last_24hr_next_day = rhumid_data.shift(periods=-1, freq=pd.Timedelta('24h')).rolling(window='24h').mean()


        dates = pd.Series(self.input_data.index.values, index=self.input_data.index)
        # Create XY data table
        xy_data_all = pd.concat([dates, in_ap_val_current, in_ap_val_prev_1hr, in_ap_val_prev_2hr,
                                 in_ap_val_prev_3hr, in_ap_val_prev_4hr, in_ap_val_prev_22hr, in_ap_val_prev_23hr,
                                 in_ap_val_prev_24hr, in_ap_val_prev_25hr, in_ap_val_prev_26hr,
                                 in_ap_avg_last_hr, in_ap_avg_last_2hr, in_ap_avg_last_3hr,
                                 in_ap_avg_last_3hr_prev_day, in_ap_val_next_22hr_prev_week, in_ap_val_next_23hr_prev_week,
                                 in_ap_val_next_24hr_prev_week, in_ap_val_next_25hr_prev_week,
                                 in_ap_val_next_26hr_prev_week, in_ap_val_current_prev_week, in_ap_avg_last_3hr_prev_week,
                                 in_ap_delta_prev_1hr_current, in_ap_delta_prev_2hr_current, in_ap_delta_prev_3hr_current,
                                 in_ap_delta_prev_23hr_prev_24hr, in_ap_delta_prev_24hr_prev_25hr,
                                 in_ap_delta_prev_25hr_prev_26hr, in_clds_val_current, in_clds_val_next_23hr,
                                 in_clds_val_next_24hr, in_clds_val_next_25hr, in_clds_avg_last_24hr_next_day,
                                 in_wspd_val_current, in_wspd_val_next_23hr, in_wspd_val_next_24hr,
                                 in_wspd_val_next_25hr, in_wspd_avg_last_24hr_next_day, in_temp_val_current,
                                 in_temp_val_next_23hr, in_temp_val_next_24hr, in_temp_val_next_25hr,
                                 in_temp_avg_last_24hr_next_day, in_rhumid_val_current,
                                 in_rhumid_val_next_23hr, in_rhumid_val_next_24hr, in_rhumid_val_next_25hr,
                                 in_rhumid_avg_last_24hr_next_day,
                                 out_ap_val_next_24hr],
                                axis=1)
        xy_data_all.columns = ['datetime', 'in_ap_val_current', 'in_ap_val_prev_1hr', 'in_ap_val_prev_2hr',
                                 'in_ap_val_prev_3hr', 'in_ap_val_prev_4hr', 'in_ap_val_prev_22hr', 'in_ap_val_prev_23hr',
                                 'in_ap_val_prev_24hr', 'in_ap_val_prev_25hr', 'in_ap_val_prev_26hr',
                                 'in_ap_avg_last_hr', 'in_ap_avg_last_2hr', 'in_ap_avg_last_3hr',
                                 'in_ap_avg_last_3hr_prev_day', 'in_ap_val_next_22hr_prev_week', 'in_ap_val_next_23hr_prev_week',
                                 'in_ap_val_next_24hr_prev_week', 'in_ap_val_next_25hr_prev_week',
                                 'in_ap_val_next_26hr_prev_week', 'in_ap_val_current_prev_week', 'in_ap_avg_last_3hr_prev_week',
                                 'in_ap_delta_prev_1hr_current', 'in_ap_delta_prev_2hr_current', 'in_ap_delta_prev_3hr_current',
                                 'in_ap_delta_prev_23hr_prev_24hr', 'in_ap_delta_prev_24hr_prev_25hr',
                                 'in_ap_delta_prev_25hr_prev_26hr', 'in_clds_val_current', 'in_clds_val_next_23hr',
                                 'in_clds_val_next_24hr', 'in_clds_val_next_25hr', 'in_clds_avg_last_24hr_next_day',
                                 'in_wspd_val_current', 'in_wspd_val_next_23hr', 'in_wspd_val_next_24hr',
                                 'in_wspd_val_next_25hr', 'in_wspd_avg_last_24hr_next_day', 'in_temp_val_current',
                                 'in_temp_val_next_23hr', 'in_temp_val_next_24hr', 'in_temp_val_next_25hr',
                                 'in_temp_avg_last_24hr_next_day', 'in_rhumid_val_current',
                                 'in_rhumid_val_next_23hr', 'in_rhumid_val_next_24hr', 'in_rhumid_val_next_25hr',
                                 'in_rhumid_avg_last_24hr_next_day', 'target']

        # Filter out the desired datetimes based on scenario preprocessing and pass back the xy_data.

        cut_bad_dates = (xy_data_all.index >= filter_column.index.values[0]) & \
                        (xy_data_all.index <= filter_column.index.values[-1])
        xy_data_all = xy_data_all[cut_bad_dates]
        xy_data_all = xy_data_all[filter_column]
        print(xy_data_all.describe())
        self.xy_data = xy_data_all
        return self.xy_data
