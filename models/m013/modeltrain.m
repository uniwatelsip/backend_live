function flag = modeltrain(model_id,tr_scenario_id,subst_id,train_data)
    % Algorithm: Simple Linear regression
    
    % The function train accepts:
    % data: training data, columns sequenced as [X Yreal]
    % model_id: string of model ID (expected: 'mXXX')
    % tr_scenario_id: string of training scenario ID (expected: 'scXXX')
    % subst_id: string of substation ID (expected: 'stXXX'
    
    % The function train:
    % 1) Saves the trained model parameters in parameters.m in directory
    %    'cwd/scXXX/stXXX'
    % 2) Returns a flag
 
    
    % Create X,Y
    y = train_data(:,end);
    X = train_data(:,1:end-1);
    
    % Model Training
    X = [X ones(length(X(:,1)),1)]; %Add a ones column for the intercept
    [b1,bint1,r1,rint1,stats1] = regress(y,X);  % Apply linear regression
    
    % Save results
    savestring = strcat('models/',model_id,'/',tr_scenario_id,'/',subst_id,...
                        '/parameters');
    save(savestring,'b1');
    flag = 'Train Complete';
end