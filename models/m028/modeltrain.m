function flag = modeltrain(model_id,tr_scenario_id,subst_id,train_data, train_seed_end)
    % Algorithm: Cross Validated MLP
    
    % The function train accepts:
    % data: training data, columns sequenced as [X Yreal]
    % model_id: string of model ID (expected: 'mXXX')
    % tr_scenario_id: string of training scenario ID (expected: 'scXXX')
    % subst_id: string of substation ID (expected: 'stXXX'
    % train_seed_end: the limit of the train seed search range.
    
    % The function train:
    % 1) Saves the trained model parameters in parameters.m in directory
    %    'cwd/scXXX/stXXX'
    % 2) Returns a flag

    % Create X,Y
    seed = 5;
    train_data = train_data(randperm(size(train_data, 1)), :);      % Shuffle the data
    y = train_data(:,end);
    X = train_data(:,1:end-1);
    [Xsc, ps_Xsc] = mapminmax(X');
    [Ysc, ps_Ysc] = mapminmax(y');
    mlp_layers = [20 10];
    mae_seed_best = 1e6;

    % Model Training
    mlp_layers_list = [20 10; 15 5; 10 5; 30 10; 25 15; 10 10];
    for list_idx = 1:size(mlp_layers_list,1)
        mlp_layers = mlp_layers_list(list_idx,:);
        for train_seed = 1:train_seed_end
            seed = train_seed;                            % Set train seed
            mlp_net = feedforwardnet(mlp_layers);         % Create MLP object
            net.divideFcn = 'divideind';
            no_obs = size(Xsc,2);                         % grab number of observations
            indices = crossvalind('KFold', no_obs, 10);   % Get indices for all k partitions


            % Cross validation for loop:
            values_notnan = 0;
            errors_sumabs = 0;
            for i = 1:10
                data_valid = find((indices == i));        % get vector with validation indices
                data_train = find(~data_valid);           % get vector with train indices
                net.divideParam.trainInd = data_train;
                net.divideParam.valInd = data_valid;
                [mlp_net, train_stats] = train(mlp_net, Xsc, Ysc);  % train the model

                % Validate the model
                x_valid = Xsc(:,data_valid);
                y_valid = Ysc(data_valid);
                yhat_valid = mlp_net(x_valid);
                yhat_valid = mapminmax('reverse', yhat_valid, ps_Ysc)';
                y_valid = mapminmax('reverse', y_valid, ps_Ysc)';
                errors_i_fold = yhat_valid - y_valid;
                values_notnan = values_notnan + nnz(~isnan(errors_i_fold));
                errors_sumabs = errors_sumabs + sumabs(errors_i_fold);
            end
            model_mae_cv = errors_sumabs / values_notnan;
            % Find best train seed MLP net by comparing MAE on train seed runs
            if model_mae_cv < mae_seed_best
                mae_seed_best = model_mae_cv;
                mlp_net_best = mlp_net;
                train_stats_best = train_stats;
                disp(strcat('current best mae: ', num2str(round(mae_seed_best,3)), '  for train seed: ', num2str(seed),...
                     'and layers: ', num2str(mlp_layers(1)),', ', num2str(mlp_layers(2))))
            end
        end
    % Save results
    savestring = strcat('models/',model_id,'/',tr_scenario_id,'/',subst_id,...
                        '/parameters');
    save(savestring,'mlp_net_best', 'train_stats_best', 'ps_Xsc', 'ps_Ysc');
    flag = 'Train Complete';
end