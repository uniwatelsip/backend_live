import numpy as np
import pandas as pd
import datetime as dt

class Preprocessor:
    def __init__(self, raw_data):

        # Find first and last occurence of True values in the filter column. Keep only values inbetween, in order to
        # avoid unnecessary string-to-datetime conversion @ datetime column (slow process).

        first_filter_loc = raw_data.loc[raw_data['Filter'] == True].index[0]
        last_filter_loc = raw_data.loc[raw_data['Filter'] == True].index[-1]
        raw_data_filter_range = raw_data[first_filter_loc-pd.Timedelta('14d'):last_filter_loc]   # grab 2 weeks extra.
        #data_index = raw_data_filter_range['SL_DateTime']
        #data_index = pd.to_datetime(data_index, format='%Y-%m-%d %H:%M:%S')
        #raw_data_filter_range.set_index(data_index, inplace=True)                    # Set datetime index
        raw_data_filter_range.sort_index(inplace=True)                               # Sort datetime index
        self.input_data = raw_data_filter_range

    def filtering(self):

        # Filter out the following:
        # 1) Datetimes outside the range of 2013-01-01 and todays date
        # 2) AP outside the range of [50,50]

        data = self.input_data
        datetime_start = dt.datetime.strptime('2013-01-01 00:00:00', '%Y-%m-%d %H:%M:%S')
        datetime_end = dt.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
        faulty_AP = (self.input_data['SL_Active'] < -50) | (self.input_data['SL_Active'] > 50)
        faulty_datetimes = (self.input_data.index < datetime_start) | (self.input_data.index > datetime_end)

        # Drop bad values
        data.mask(faulty_AP, np.nan, inplace=True)
        data = data.drop(data[faulty_datetimes].index, axis=1)
        # Remove duplicate indices (if any)
        data = data.loc[~data.index.duplicated(keep='first')]
        data.dropna(subset=['SL_DateTime'], inplace=True)
        data.sort_index(inplace=True)
        self.input_data = data

    def resampling(self):
        data = self.input_data[['SL_Active', 'SL_Reactive', 'SL_Voltage', 'SL_ir3_Tap', 'Wt_Clouds', 'Wt_Speed',
                                'Wt_Temp', 'Filter']]
        # Make sure Filter column is boolean
        filter_col = data.pop('Filter')
        filter_col = filter_col == True
        data['Filter'] = filter_col


        resamp_freq = '5min'
        resamp_aggr = data.resample(resamp_freq, loffset=resamp_freq, closed='right').agg(['mean', 'count'])
        filter_column = filter_col.resample(resamp_freq).fillna('backfill', limit=10)
        invalid = resamp_aggr['SL_Active']['count'] <= 2
        # Render the invalid measurements to NaNs
        resamp_aggr[invalid] = np.nan
        # This "if" robustifies the resample process: if the preprocess algorithm comes across
        # data with problematic sampling rates (as in, greater than 10', compared to the usual 1'),
        # then upsample original data to 1' and interpolate inbetween.
        if resamp_aggr['SL_Active']['mean'].dropna().shape[0] == 0:
            data = data.resample('1min').interpolate(method='linear')
            resamp_aggr = data.resample(resamp_freq, loffset=resamp_freq, closed='right').agg(['mean', 'count'])
            invalid = resamp_aggr['SL_Active']['count'] <= 2
            # Render the invalid measurements to NaNs
            resamp_aggr[invalid] = np.nan

        # Grab the columns and create a new dataframe, avg'd @ 15mins.
        data_resamp = pd.DataFrame(
            {'SL_Active': resamp_aggr['SL_Active']['mean'],
             'hd_clouds': resamp_aggr['Wt_Clouds']['mean'],
             'hd_temperature': resamp_aggr['Wt_Temp']['mean'],
             'hd_wind_speed': resamp_aggr['Wt_Speed']['mean'],
             'Filter': filter_column
             })
        self.input_data = data_resamp


    def xy_creation(self):
        timestep = pd.Timedelta('24h')  # Model Timestep

        ap_data = self.input_data['SL_Active']
        cloud_data = self.input_data['Wt_Clouds']
        temp_data = self.input_data['Wt_Temp']
        wind_data = self.input_data['Wt_Speed']
        filter_column = self.input_data['Filter'] == 1
        filter_column_stripped = filter_column.reindex(filter_column.index.round('D'))
        filter_column_stripped = filter_column_stripped.loc[~filter_column_stripped.index.duplicated(keep='first')]
        filter_column_stripped.dropna(inplace=True)
        filter_column_stripped = filter_column_stripped[filter_column_stripped == True]
        # XY inputs creation

        # ---- AP Inputs ----
        out_ap_max_next_24hr = ap_data.shift(periods=-1, freq=timestep).rolling(window='24h').max()  # Model Output

        in_ap_max_current_24hr = ap_data.rolling(window='24h').max()
        in_ap_max_prev_day = ap_data.shift(periods=1, freq=timestep).rolling(window='24h').max()
        in_ap_max_prev_2days = ap_data.shift(periods=2, freq=timestep).rolling(window='24h').max()
        in_ap_max_prev_3days = ap_data.shift(periods=3, freq=timestep).rolling(window='24h').max()
        in_ap_max_prev_week = ap_data.shift(periods=7, freq=timestep).rolling(window='24h').max()

        in_ap_delta_prev_2days_current = in_ap_max_prev_2days - in_ap_max_current_24hr
        in_ap_delta_prev_3days_current = in_ap_max_prev_3days - in_ap_max_current_24hr

        # ---- Weather Inputs -----
        in_clds_avg_current = cloud_data.rolling(window='24h').mean()
        in_clds_val_next_9hr = cloud_data.shift(periods=-9, freq=pd.Timedelta('1hr'))
        in_clds_val_next_12hr = cloud_data.shift(periods=-12, freq=pd.Timedelta('1hr'))
        in_clds_val_next_15hr = cloud_data.shift(periods=-15, freq=pd.Timedelta('1hr'))
        in_clds_avg_next_day = cloud_data.shift(periods=-1, freq=timestep).rolling(window='24h').mean()

        in_wspd_avg_current = wind_data.rolling(window='24h').mean()
        in_wspd_val_next_9hr = wind_data.shift(periods=-9, freq=pd.Timedelta('1hr'))
        in_wspd_val_next_12hr = wind_data.shift(periods=-12, freq=pd.Timedelta('1hr'))
        in_wspd_val_next_15hr = wind_data.shift(periods=-15, freq=pd.Timedelta('1hr'))
        in_wspd_avg_next_day = wind_data.shift(periods=-1, freq=timestep).rolling(window='24h').mean()

        in_temp_avg_current = temp_data.rolling(window='24h').mean()
        in_temp_val_next_9hr = temp_data.shift(periods=-9, freq=pd.Timedelta('1hr'))
        in_temp_val_next_12hr = temp_data.shift(periods=-12, freq=pd.Timedelta('1hr'))
        in_temp_val_next_15hr = temp_data.shift(periods=-15, freq=pd.Timedelta('1hr'))
        in_temp_avg_next_day = temp_data.shift(periods=-1, freq=timestep).rolling(window='24h').mean()


        dates = pd.Series(self.input_data.index.values, index=self.input_data.index)
        # Create XY data table
        xy_data_all = pd.concat([dates, in_ap_max_current_24hr, in_ap_max_prev_day, in_ap_max_prev_2days, in_ap_max_prev_week,
                                 in_ap_delta_prev_2days_current, in_ap_delta_prev_3days_current, in_clds_avg_current,
                                 in_clds_val_next_9hr, in_clds_val_next_12hr, in_clds_val_next_15hr, in_clds_avg_next_day,
                                 in_wspd_avg_current, in_wspd_val_next_9hr, in_wspd_val_next_12hr, in_wspd_val_next_15hr,
                                 in_wspd_avg_next_day, in_temp_avg_current, in_temp_val_next_9hr, in_temp_val_next_12hr,
                                 in_temp_val_next_15hr, in_temp_avg_next_day, out_ap_max_next_24hr],
                                axis=1)
        xy_data_all.columns = ['datetime', 'in_ap_max_current_24hr', 'in_ap_max_prev_day', 'in_ap_max_prev_2days', 'in_ap_max_prev_week',
                                 'in_ap_delta_prev_2days_current', 'in_ap_delta_prev_3days_current', 'in_clds_avg_current',
                                 'in_clds_val_next_ 9hr', 'in_clds_val_next_12hr', 'in_clds_val_next_15hr', 'in_clds_avg_next_day',
                                 'in_wspd_avg_current', 'in_wspd_val_next_9hr', 'in_wspd_val_next_12hr', 'in_wspd_val_next_15hr',
                                 'in_wspd_avg_next_day', 'in_temp_avg_current', 'in_temp_val_next_9hr', 'in_temp_val_next_12hr',
                                 'in_temp_val_next_15hr', 'in_temp_avg_next_day', 'target']

        # Reindex Filter column to day granularity: First, find True values in whatever time of the day, and keep only
        # those. Normalize the datetime index (floor all indices to midnight time), and then drop duplicate indices. In
        # the end, make sure all values are True. Now we have a filter column which highlights all days where originally
        # at least one True value existed. Basically, a weekdays_daylight scenario is equal to a weekdays_night scenario
        filter_column = filter_column[filter_column == True]
        filter_column_stripped = filter_column.reindex(filter_column.index.normalize())
        filter_column_stripped = filter_column_stripped.loc[~filter_column_stripped.index.duplicated(keep='first')]
        filter_column_stripped.values[:] = True

        # Reindex xy_data_all dataframe to day granularity. Keep only the midnight time values. This corresponds to 1
        # input datapoint per day.
        xy_data_all = xy_data_all.reindex(xy_data_all.index.normalize())
        xy_data_all_stripped = xy_data_all.loc[~xy_data_all.index.duplicated(keep='first')]

        # Filter out the desired days based on scenario preprocessing and pass back the xy_data.
        cut_bad_dates = (xy_data_all_stripped.index >= filter_column_stripped.index.values[0]) & \
                        (xy_data_all_stripped.index <= filter_column_stripped.index.values[-1])
        xy_data_all_stripped = xy_data_all_stripped.iloc[cut_bad_dates]
        xy_data_all_stripped = xy_data_all_stripped.loc[filter_column_stripped.index.values]
        xy_data_all_stripped.dropna(inplace=True)
        print(xy_data_all_stripped.describe())
        self.xy_data = xy_data_all_stripped
        return self.xy_data
