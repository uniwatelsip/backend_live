function [r2,rmse,mare,MAE,errorvar] = tseries_stats(yreal,yhat,varargin)
% Compute coefficient of determination of data fit model and RMSE

% RSQUARE computes the coefficient of determination (R-square) value from
% actual data Y and model data F. The code uses a general version of 
% R-square, based on comparing the variability of the estimation errors 
% with the variability of the original values. RSQUARE also outputs the
% root mean squared error (RMSE) for the user's convenience.
%
% Note: RSQUARE ignores comparisons involving NaN values.
% 
% INPUTS
%   yreal       : Actual data
%   yhat        : Model fit
%
% OPTION
%   C       : Constant term in model
%             R-square may be a questionable measure of fit when no
%             constant term is included in the model.
%   [DEFAULT] TRUE : Use traditional R-square computation
%            FALSE : Uses alternate R-square computation for model
%                    without constant term [R2 = 1 - NORM(Y-F)/NORM(Y)]
%   
% Jered R Wells
% 11/17/11
% jered [dot] wells [at] duke [dot] edu
%
% v1.2 (02/14/2012)
%
% Thanks to John D'Errico for useful comments and insight which has helped
% to improve this code. His code POLYFITN was consulted in the inclusion of
% the C-option (REF. File ID: #34765).

if isempty(varargin); c = true; 
elseif length(varargin)>1; error 'Too many input arguments';
elseif ~islogical(varargin{1}); error 'C must be logical (TRUE||FALSE)'
else c = varargin{1}; 
end

% Compare inputs
if ~all(size(yreal)==size(yhat)); error 'Y and F must be the same size'; end

% Check for NaN
tmp = ~or(isnan(yreal),isnan(yhat));
yreal = yreal(tmp);
yhat = yhat(tmp);

if c; r2 = max(0,1 - sum((yreal(:)-yhat(:)).^2)/sum((yreal(:)-mean(yreal(:))).^2));
else r2 = 1 - sum((yreal(:)-yhat(:)).^2)/sum((yreal(:)).^2);
    if r2<0
    % http://web.maths.unsw.edu.au/~adelle/Garvan/Assays/GoodnessOfFit.html
        warning('Consider adding a constant term to your model') %#ok<WNTAG>
        r2 = 0;
    end
end
% Calculate Root Mean Squared Error
rmse = sqrt(mean((yreal(:) - yhat(:)).^2));

% Calculate Mean Absolut Relative Error
mare=sum(abs((yreal-yhat)./yreal))/size(yhat,1);

% Calculate Mean Absolut Error
MAE=sum(abs(yreal-yhat ))/size(yhat,1);

% Calculate the Error Variance
errorvar=var(yreal-yhat);