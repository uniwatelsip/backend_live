def data():

    import pandas as pd
    import numpy as np

    data = pd.read_csv('dummyset_weather.csv', false_values='NULL')
    data_index = pd.to_datetime(data['SL_DateTime'], format='%d-%m-%y %H:%M')
    start_date = np.datetime64('2018-01-01 00:00')
    end_date = np.datetime64('2018-04-18 09:07')
    filter_period = (data_index > start_date) & \
                    (data_index < end_date)
    filter_period.rename('Filter')
    data['Filter'] = filter_period
    data.to_csv('dummyset_weather2.csv')

def preprocess():

    import pandas as pd
    from preprocessor import Preprocessor

    data = pd.read_csv('st000_sc018_03.csv', false_values='NULL')

    preprocessor = Preprocessor(data)
    preprocessor.filtering()
    preprocessor.resampling()
    preprocessor.xy_creation()

def check_shift():

    import pandas as pd
    import numpy as np

    data = pd.read_csv('resampled_data.csv', false_values='NULL')
    data_index = pd.to_datetime(data['SL_DateTime'], format='%Y-%m-%d %H:%M:%S')
    data.set_index(data_index, inplace=True)

    ap_data = data['SL_Active']
    cloud_data = data['hd_clouds']
    temperature_data = data['hd_temperature']
    wind_data = data['hd_wind_speed']

    timestep = pd.Timedelta('15min')                                  # Model Timestep
    out_ap_val_next_15min = ap_data.shift(periods=-1, freq=timestep)  # Model Output

    in_ap_val_current = ap_data
    in_ap_val_prev_15min = ap_data.shift(periods=1, freq=timestep)
    in_ap_avg_last_hr = ap_data.rolling(window='1h').mean()
    in_ap_avg_last_2hr = ap_data.rolling(window='2h').mean()
    in_ap_avg_last_3hr = ap_data.rolling(window='3h').mean()
    in_ap_avg_last_4hr = ap_data.rolling(window='4h').mean()
    in_ap_avg_last_day = ap_data.rolling(window='24h').mean()

    in_ap_val_next_15min_prev_day = ap_data.shift(periods=1, freq=pd.Timedelta('1d')-timestep)
    in_ap_avg_last_hr_prev_day = ap_data.shift(periods=1, freq=pd.Timedelta('1d')).rolling(window='1h').mean()
    in_ap_val_next_15min_prev_week = ap_data.shift(periods=1, freq=pd.Timedelta('7d')-timestep)
    in_ap_avg_last_hr_prev_week = ap_data.shift(periods=1, freq=pd.Timedelta('7d')).rolling(window='1h').mean()

    in_ap_delta_prev_15min_current = ap_data - ap_data.shift(1)
    in_ap_delta_prev_hr_current = ap_data - ap_data.shift(periods=1, freq=pd.Timedelta('1h'))
    in_ap_delta_prev_2hr_current = ap_data - ap_data.shift(periods=1, freq=pd.Timedelta('2h'))
    in_ap_max_last_day = ap_data.rolling(window='24h').max()

    dates = pd.Series(data_index, index=data_index)
    # Create XY data table
    xy_data = pd.concat([dates, in_ap_val_current,
                             in_ap_val_prev_15min, in_ap_avg_last_hr,
                             in_ap_val_next_15min_prev_day, in_ap_avg_last_hr_prev_day,
                             in_ap_val_next_15min_prev_week, in_ap_avg_last_hr_prev_week,
                             in_ap_avg_last_2hr, in_ap_avg_last_3hr,
                             in_ap_avg_last_4hr, in_ap_avg_last_day,
                             in_ap_delta_prev_15min_current, in_ap_delta_prev_hr_current,
                             in_ap_delta_prev_2hr_current, in_ap_max_last_day,
                             out_ap_val_next_15min], axis=1)
    xy_data.columns = ['datetime', 'in_ap_val_current',
                             'in_ap_val_prev_15min', 'in_ap_avg_last_hr',
                             'in_ap_val_next_15min_prev_day', 'in_ap_avg_last_hr_prev_day',
                             'in_ap_val_next_15min_prev_week', 'in_ap_avg_last_hr_prev_week',
                             'in_ap_avg_last_2hr', 'in_ap_avg_last_3hr',
                             'in_ap_avg_last_4hr', 'in_ap_avg_last_day',
                             'in_ap_delta_prev_15min_current', 'in_ap_delta_prev_hr_current',
                             'in_ap_delta_prev_2hr_current', 'in_ap_max_last_day',
                             'target']
    test=1

def drop_xy_nans():

    import pandas as pd
    data = pd.read_csv('m001_sc018_02_train.csv', false_values='NULL')
    data.dropna(how='any', axis=0, inplace=True)
    data.to_csv('m001_xy_nonans.csv')
    data.drop(['in_clds_val_current', 'in_wspd_val_current', 'in_temp_val_current'], axis=1, inplace=True)
    data.to_csv('m000_xy_nonans.csv')
    print('lol')

def compare_xy():

    import pandas as pd
    desired_width = 520
    pd.set_option('display.width', desired_width)
    pd.set_option('display.max_columns', 10)
    m000_xy = pd.read_csv('m000_xy.csv', false_values='NULL')
    m000_xy_nonans = pd.read_csv('m000_xy_nonans.csv', false_values='NULL')
    describe_m000_xy = m000_xy.describe()
    describe_m000_xy_nonans = m000_xy_nonans.describe()
    print('m000_xy_nonans')
    print(describe_m000_xy_nonans)
    print('m000_xy')
    print(describe_m000_xy)
if __name__ == "__main__":

    drop_xy_nans()