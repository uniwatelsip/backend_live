function yhat = modeleval(model_id,tr_scenario_id,test_scenario_id,subst_id,...
                          test_data)
    % Algorithm: Simple Linear regression
    
    % The function eval reads:
    % data: testing data, columns sequenced as [X Yreal]
    % model_id: string of model ID (expected: 'mXXX')
    % tr_scenario_id: string of training scenario ID (expected: 'scXXX')
    % test_scenario_id: string of test scenario ID (expected: 'tXXX')
    % subst_id: string of substation ID (expected: 'stXXX')
    
    % The function eval:
    % 1) Loads the parameters.m file from cwd/scXXX/stXXX
    % 2) Saves the results in
    %    'cwd/scXXX/stXXX/res/mXXX_scXXX_stXXX_tXXX.csv'
    % 3) Returns the yhat predictions

    parameter_string = strcat('models/',model_id,'/',tr_scenario_id,'/',subst_id,...
                              '/parameters');
    model_params = load(parameter_string);
    linear_coeffs = model_params.('b1');
    %
    
    % Create X,Y
    y = test_data(:,end);
    X = test_data(:,1:end-1);
    
    % Model Evaluation
    X = [X ones(length(X(:,1)),1)];
    yhat = X*linear_coeffs;  % Compute Predictions
    disp(length(yhat))
end
    