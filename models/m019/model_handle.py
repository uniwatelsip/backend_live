import matlab.engine
import numpy as np
import os
import importlib
import pandas as pd
from .preprocessor import Preprocessor


class Model_Handle:

    def __init__(self, config, matlabeng):

        self.model_id = config.get('model_id')                     # Get model ID (expected: 'mXXX')
        self.train_scenario_id = config.get('train_scenario')      # Get train scenario ID (expected: 'scXXX')
        self.test_scenario_id = config.get('test_scenario')        # Get test scenario ID (expected: 'tXXX')
        self.substation_id = config.get('substation_id')           # Get substation ID (expected:'stXXX')
        self.abs_path = os.path.abspath(config.get('model_path'))  # Find absolute path of Model_Handle (for matlab eng)
        self.matlabeng = matlabeng                                 # Get matlab engine API module

    def xy_creation(self, input_data, train_flag):

        # Import preprocessor and pass data
        preprocessor = Preprocessor(input_data)
        xy_data = preprocessor.xy_creation()
        return xy_data

    def run_train(self, xy_data):

        xy_data.drop('datetime', axis=1, inplace=True)
        xy_data_mat = matlab.double(xy_data.values.tolist())  # Change xy_data from df to matlab array.

        matlabeng = self.matlabeng
        matlabeng.addpath(self.abs_path)                      # Add model directory so that modeltrain is discoverable

        # Run matlab model
        flag = matlabeng.modeltrain(self.model_id, self.train_scenario_id, self.substation_id, xy_data_mat)
        return flag

    def run_eval(self, xy_data):

        datetime = xy_data['datetime']
        xy_data.drop('datetime', axis=1, inplace=True)
        xy_data_mat = matlab.double(xy_data.values.tolist())  # Change xy_data from df to matlab array.

        matlabeng = self.matlabeng
        matlabeng.addpath(self.abs_path)                      # Add model directory so that modeleval is discoverable

        # Run matlab model
        yhat = matlabeng.modeleval(self.model_id, self.train_scenario_id, self.test_scenario_id, self.substation_id,
                                   xy_data_mat)
        yhat = np.asarray(yhat._data)                  # convert yhat from ml.engine array to numpy array
        yreal = xy_data['target'].values
        results = pd.DataFrame({'yreal': yreal.tolist(), 'yhat': yhat.tolist()}, index=datetime)

        res_path = '{!s}/{!s}/{!s}/res'.format(self.abs_path, self.train_scenario_id, self.substation_id)
        filename = '{!s}/{!s}_{!s}_{!s}_{!s}.csv'.format(res_path, self.model_id, self.train_scenario_id,
                                                         self.substation_id, self.test_scenario_id)

        # Save the results in the backend only if the run is not a direct evaluation.
        if self.test_scenario_id != 'direct_eval':
            results.to_csv(filename)

        return results
