"""
Telsip Server
"""

import requests
from flask import Flask, request, make_response, jsonify
import pandas as pd
from telsipbackend import TelsipBackend
import matlab.engine
from datetime import datetime
import logging
import threading

app = Flask(__name__)
mateng = matlab.engine.start_matlab()              # Start matlab engine
lock = threading.Lock()

# Create logger
server_logger = logging.getLogger('server_logger')
server_timestamp = datetime.now().strftime('%Y-%m-%d_%H_%M_%S')
filename_string = 'serverlog.log'
# Create handlers
c_handler = logging.StreamHandler()
f_handler = logging.FileHandler('server.log')
c_handler.setLevel(logging.INFO)
f_handler.setLevel(logging.WARNING)
# Create formatters and add it to handlers
c_format = logging.Formatter('%(asctime)s - %(message)s')
f_format = logging.Formatter('%(asctime)s - %(message)s')
c_handler.setFormatter(c_format)
f_handler.setFormatter(f_format)
# Add handlers to the logger
server_logger.addHandler(c_handler)
server_logger.addHandler(f_handler)
# log server timestamp start
server_logger.warning('Started Server @ {!s}'.format(server_timestamp))


@app.route("/is_online")
def is_online():
    """
    Checks if the server is online.
    Accepts: -
    Returns: python dict
    """
    return "Server is Online", 200


@app.route("/upload_dataset", methods=['POST'])
def upload_dataset():
    """
    Uploads a dataset to the server, and stores it in the local database.
    Accepts: an http post request containing a jsonified dict with the following data:
             {'substation_id': subst id, 'daterange': daterange, 'dataset': dataset data}
             The dataset is a dataframe converted to dict (df.to_dict)
    Returns: String
    """
    post_data = request.get_json()
    dataset = pd.DataFrame.from_dict(post_data.get('dataset'), orient='columns')
    dataset_name = post_data.get('substation_id')
    backend = TelsipBackend({}, {}, lock, server_logger)
    msg = backend.store_dataset(dataset, dataset_name)

    return_msg = "Dataset Uploaded and stored as {!s}".format(dataset_name)
    return return_msg, 200


@app.route("/upload_scenario", methods=['POST'])
def upload_scenario():
    """
    Uploads a scenario to the server, and stores it in the local database.
    Accepts: an http post request containing a jsonified dict with the following data:
             {'scenario_id': scenario, 'daterange': "2013-2018", 'scenario': scenario data}
             The scenario is a dataframe converted to dict (df.to_dict)
    Returns: String
    """

    post_data = request.get_json()
    # Grab scenario from json string
    scenario = pd.DataFrame.from_dict(post_data.get('scenario'), orient='columns')
    scenario_name = post_data.get('scenario_id')
    backend = TelsipBackend({}, {}, lock, server_logger)
    msg = backend.store_scenario(scenario, scenario_name)

    return_msg = "Scenario Uploaded and stored as {!s}".format(scenario_name)
    return return_msg, 200


@app.route("/train_model")
def train_model():
    """
    Trains a model, given a model id, a train scenario ID, and a substation ID.
    Accepts: an http request with the following config:
             config = {'model ID': "m001", 'train scenario': "sc018_02", 'substation ID': "st000", 'overwrite_model':
             False}
    Returns: A jsonified string containing a range of coded messages (see readme)
    """

    # Create config file to pass to backend
    config = request.json
    config['request_timestamp'] = datetime.now().time().strftime('%H:%M:%S')
    # Initialize Backend, pass data + config
    backend = TelsipBackend(config, mateng, lock, server_logger)
    # Run train
    msg = backend.train()
    msg_json = jsonify(msg)
    return msg_json


@app.route("/eval_model")
def eval_model():
    """
    Εvaluates a model, given a model id, a train scenario ID, a substation ID and a test scenario ID.
    Accepts: an http request with the following config:
             config = {'model ID': "m001", 'train scenario': "sc018_02", 'substation ID': "st000", 'test scenario':
             "t018_02", overwrite_run: True}
    Returns: In the case of invalid request, a jsonified string containing a range of coded messages (see readme)
             In the case of a successful evaluation, a jsonified dataframe containing the results of the
             evaluation.
    """

    # Create config file to pass to backend
    config = request.json
    config['request_timestamp'] = datetime.now().time().strftime('%H:%M:%S')
    # Initialize Backend, pass data + config
    backend = TelsipBackend(config, mateng, lock, server_logger)
    # Run eval
    output = backend.eval()
    if isinstance(output, pd.DataFrame):
        eval_response = output.to_json(orient='split')
    else:
        eval_response = jsonify(output)
    return eval_response


@app.route("/direct_eval", methods=['POST'])
def direct_eval():
    """
    Uploads a dataset for direct evaluation from a trained model.
    Accepts: an http post request containing a jsonified dict with the following data:
             {'data': eval data, 'model_id': "m001", 'train_scenario': "sc018_02", 'substation_id': "st000046}
             The eval data is a dataframe converted to dict (df.to_dict)
    Returns: In the case of invalid request, a jsonified string containing a range of coded messages (see readme)
             In the case of a successful evaluation, returns a jsonified dataframe containing the results of the
             evaluation.
    """

    post_data = request.get_json()
    # Grab eval data from json string
    test_data = pd.DataFrame.from_dict(post_data.get('test_data'), orient='columns')
    train_scenario = post_data.get('train_scenario')
    model_id = post_data.get('model_id')
    substation_id = post_data.get('substation_id')
    # Arrange them in config dict to pass to the backend.
    config = {'train_scenario': train_scenario, 'model_id': model_id, 'substation_id': substation_id,
              'test_scenario': "direct_eval", 'request_timestamp': datetime.now().time().strftime('%H:%M:%S')}
    # Initialize backend, pass data + config
    backend = TelsipBackend(config, mateng, lock, server_logger)
    # Run direct evaluation
    output = backend.direct_eval(test_data)
    if isinstance(output, pd.DataFrame):
        eval_response = output.to_json(orient='split')
    else:
        eval_response = jsonify(output)
    return eval_response


@app.route("/outlier_detection")
def outlier_detection():
    """
    Detects outliers in a stored dataset, given an outlier detection method and its' parameters.
    Accepts: an http post request containing a jsonified dict with the following data:
             {'od_method': see matlab> isoutlier, 'mov_window': in minutes, 'threshold_factor': mean absolute deviations,
             'substation_id': "st00003", 'date_from': datestring, 'date_to':datestring}
    Returns: In the case of invalid substation request, an error message
             In the case of a successful evaluation, returns a jsonified dataframe containing the results of the
             outlier detection procedure.
    """
    # Create config file to pass to backend
    config = request.json
    config['request_timestamp'] = datetime.now().time().strftime('%H:%M:%S')
    # Initialize Backend, pass data + config
    backend = TelsipBackend(config, mateng, lock, server_logger)
    # Run eval
    output = backend.outlier_detection(config)
    if isinstance(output, pd.DataFrame):
        eval_response = output.to_json(orient='split')
    else:
        eval_response = jsonify(output)
    return eval_response

@app.route("/data_grab")
def data_grab():
    """
    Filters and preprocess data using the preprocessor.py of a given model. Returns the XY data to client.
    Accepts: an http post request containing a jsonified dict with the following data:
             config = {'model ID': "m001", 'train scenario': "sc018_02", 'substation ID': "st000", 'test scenario':
             "t018_02", overwrite_run: True}
    Returns: In the case of invalid substation request, an error message
             In the case of a successful evaluation, returns a jsonified dataframe containing the results of the
             outlier detection procedure.
    """
    # Create config file to pass to backend
    config = request.json
    config['request_timestamp'] = datetime.now().time().strftime('%H:%M:%S')
    # Initialize Backend, pass data + config
    backend = TelsipBackend(config, mateng, lock, server_logger)
    # Run eval
    output = backend.data_grab()
    if isinstance(output, pd.DataFrame):
        eval_response = output.to_json(orient='split')
    else:
        eval_response = jsonify(output)
    return eval_response

@app.route("/res_grab")
def res_grab():
    """
    Grabs results of a run, given a model id, a train scenario ID, a substation ID and a test scenario ID.
    Accepts: an http request with the following config:
             config = {'model ID': "m001", 'train scenario': "sc018_02", 'substation ID': "st000",
                       'test scenario': "t018_02", 'from': "2018-01-01 07:20:00", to: "2019-01-01 07:20:00"}
    Returns: In the case of invalid request, a jsonified string containing "not_found"
             In the case of a successful evaluation, returns a jsonified dataframe containing the requested results
    """
    config = request.json

    config['request_timestamp'] = datetime.now().time().strftime('%H:%M:%S')
    # Initialize Backend, pass data + config
    backend = TelsipBackend(config, mateng, lock, server_logger)

    # Run res_grab
    output = backend.res_grab()
    if isinstance(output, pd.DataFrame):
        eval_response = output.to_json(orient='split')
    else:
        eval_response = jsonify(output)
    return eval_response


@app.route("/model_list")
def model_list():
    """
    Returns: The paths.yaml file, so that the client can see what the available models, datasets and scenarios are.
    """
    backend = TelsipBackend({}, {}, lock, server_logger)
    output = backend.model_list()
    output_json = jsonify(output)
    return output_json


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': "https://media.giphy.com/media/hEc4k5pN17GZq/giphy.gif"}), 404)


if __name__ == "__main__":
    app.run(debug=False, threaded=True, host="0.0.0.0", port=8081)

