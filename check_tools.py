import pandas as pd
import os
from shutil import rmtree


class CheckTools:
    def __init__(self, check_config, logger):
        self.check_config = check_config
        # Load paths and IDs
        self.model_id = self.check_config.get('model_id')
        self.model_infos = self.check_config.get('model_infos')
        self.model_info = self.model_infos.get(self.model_id)
        self.train_scenario_id = self.check_config.get('train_scenario')
        self.test_scenario_id = self.check_config.get('test_scenario')
        self.substation_id = self.check_config.get('substation_id')
        self.model_paths = self.check_config.get('model_paths')                    # Grab model_paths dict
        self.trained_model_paths = self.check_config.get('trained_model_paths')    # Grab trained model paths
        self.model_path = self.model_paths.get(self.model_id)
        self.scenario_paths = self.check_config.get('scenario_paths')
        self.dataset_paths = self.check_config.get('dataset_paths')
        self.overwrite_model = self.check_config.get('overwrite_model')
        self.overwrite_run = self.check_config.get('overwrite_run')
        # load misc information
        self.logger_sentence = self.check_config.get('logger_sentence')
        self.timestamp = check_config.get('request_timestamp')
        self.check_config = check_config
        self.server_logger = logger

    def check_train(self):
        # This checks whether a model with the given train scenario and substation already exists.
        # (Reminder: a train scenario and a substation constitute a unique trained model.)

        # Is the requested model ID in the model_paths dict?
        if self.model_id in self.model_paths:
            # Does the corresponding scenario folder exist in trained_model_paths?
            model_scenario_folder = self.trained_model_paths.get(self.model_id, {})
            if model_scenario_folder is None:
                output_msg = 'Model_ID_exists'
            elif self.train_scenario_id in model_scenario_folder:
                # Does a trained model for the given substation exist?
                if self.substation_id in model_scenario_folder.get(self.train_scenario_id, {}):
                    output_msg = 'Trained_Model_ID_exists'
                    if self.overwrite_model:
                        # If the overwrite parameter is true, then revert output msg back to model_id_exists, so that
                        # the training function can be activated regardless.
                        trained_model_path = '{!s}/{!s}/{!s}'.format(self.model_path, self.train_scenario_id,
                                                                     self.substation_id)
                        if os.path.exists(trained_model_path):
                            rmtree(trained_model_path)
                        output_msg = 'Model_ID_exists'
                else:
                    output_msg = 'Model_ID_exists'
            else:
                output_msg = 'Model_ID_exists'
        else:
            output_msg = 'Model_ID_nonexistent'

        # If model id exists, then check if the scenario_id exists in the telsip database:
        train_scenario_stripped = 's' + self.train_scenario_id[2:]
        if output_msg == 'Model_ID_exists':
            scenario_id = self.scenario_paths.get(train_scenario_stripped)
            if scenario_id is None:
                output_msg = 'Model_ID_exists__Scenario_ID_nonexistent'
            else:
                output_msg = 'Model_ID_+_scenario_ID_exist'

        # If model id and scenario ID exist, then check if the dataset exists in the telsip database:
        if output_msg == 'Model_ID_+_scenario_ID_exist':
            subst_id = self.dataset_paths.get(self.substation_id)
            if subst_id is None:
                output_msg = 'Model_ID_+_scenario_ID_exist__subst_ID_nonexistent'
            else:
                output_msg = 'Model_ID_+_scenario_ID_+_subst_ID_exist'

        self.server_logger.warning('{!s}  {!s}'.format(self.logger_sentence, output_msg))
        return output_msg

    def check_eval(self):
        # This check structure checks whether a model with the given train scenario and substation already exists.
        # (Reminder: a train scenario and a substation constitute a unique trained model.)

        # Is the requested model ID in the model_paths dict?
        if self.model_id in self.model_paths:
            # Does the corresponding scenario folder exist in trained_model_paths?
            model_scenario_folder = self.trained_model_paths.get(self.model_id, {})
            if model_scenario_folder is None:
                output_msg = 'Model_ID_exists'
            elif self.train_scenario_id in model_scenario_folder:
                # Does a trained model for the given substation exist?
                if self.substation_id in model_scenario_folder.get(self.train_scenario_id, {}):
                    output_msg = 'Trained_Model_ID_exists'
                else:
                    output_msg = 'Model_not_trained_on_subst_ID'
            else:
                output_msg = 'Model_not_trained_on_scenario'
        else:
            output_msg = 'Model_ID_nonexistent'

        # If trained model id exists, then check if the test scenario id exists in the telsip database:
        test_scenario_stripped = 's' + self.test_scenario_id[1:]
        if output_msg == 'Trained_Model_ID_exists':
            scenario_id = self.scenario_paths.get(test_scenario_stripped)
            if scenario_id is None:
                output_msg = 'Trained_Model_ID_exists__Scenario_ID_nonexistent'
            else:
                output_msg = 'Trained_Model_ID_+_scenario_ID_exist'

        # If trained model id and scenario ID exist, then check if the dataset exists in the telsip database:
        if output_msg == 'Trained_Model_ID_+_scenario_ID_exist':
            subst_id = self.dataset_paths.get(self.substation_id)
            if subst_id is None:
                output_msg = 'Trained_Model_ID_+_scenario_ID_exist__subst_ID_nonexistent'
            else:
                output_msg = 'Trained_Model_ID_+_scenario_ID_+_subst_ID_exist'
                # Only in this case is eval possible.

        # If all the above checks are valid, finally check if the run has already been evaluated. If the overwrite_run
        # parameter is true, then ignore the check and evaluate the run regardless, thus overwriting the old one. This
        # is not applicable to direct_eval evaluations, that's why a check in the if condition exists.
        if (output_msg == 'Trained_Model_ID_+_scenario_ID_+_subst_ID_exist') & (self.overwrite_run is False) & \
           (self.test_scenario_id != 'direct_eval'):
            res_path = self.trained_model_paths.get(self.model_id).get(self.train_scenario_id).get(self.substation_id)
            csv_name = '{!s}_{!s}_{!s}_{!s}'.format(self.model_id, self.train_scenario_id, self.substation_id,
                                                    self.test_scenario_id)
            csv_path = './{!s}/res/{!s}.csv'.format(res_path, csv_name)
            if os.path.isfile(csv_path):
                output_msg = 'run_already_exists:{!s}'.format(csv_name)
        self.server_logger.warning('{!s}  {!s}'.format(self.logger_sentence, output_msg))
        return output_msg

    def nan_threshold_check(self, data, active_columns, nan_threshold):
        data['Filter'] = data['Filter'] == True
        df_nan_check = data.resample('1min', loffset='1min', closed='right').mean()
        df_nan_check = df_nan_check[df_nan_check['Filter'] == True]
        df_nan_check_length = len(df_nan_check.index)
        nan_rows = df_nan_check_length - df_nan_check[active_columns].count()
        if isinstance(nan_rows, pd.Series):
            nan_rows = nan_rows.max()
        nan_percent = nan_rows / df_nan_check_length
        if nan_percent < nan_threshold:
            msg = "nan_percent_below_threshold"
        else:
            msg = "nan_percent_over_threshold"
        self.server_logger.warning('{!s}  {!s}'.format(self.logger_sentence, msg))
        return msg

    def filter_datetime_check(self, scenario, dataset):
        filter_true = scenario.loc[scenario['Filter'] == True]
        first_filter_iloc = filter_true.index[0]
        last_filter_iloc = filter_true.index[-1]
        if (pd.to_datetime(dataset.index[0]) <= pd.to_datetime(first_filter_iloc)) & \
           (pd.to_datetime(dataset.index[-1]) >= pd.to_datetime(last_filter_iloc)):
            msg = "Supplied_filter_within_datetime_bounds"
        else:
            msg = "Supplied_filter_out_of_datetime_bounds"
        self.server_logger.warning('{!s}  {!s}'.format(self.logger_sentence, msg))
        return msg

    @staticmethod
    def xy_data_check(xy_data):
        if xy_data.shape[0] > 0:
            msg = 'xy_data_valid'
        else:
            msg = 'xy_data_zero'
        return msg
