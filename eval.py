import importlib
from check_tools import CheckTools
from sklearn import metrics

class Eval:
    def __init__(self, config, mateng, check_tools, data_tools):
        self.model_paths = config.get('model_paths')
        self.trained_model_paths = config.get('trained_model_paths')
        self.scenario_paths = config.get('scenario_paths')
        self.dataset_paths = config.get('dataset_paths')
        self.model_id = config.get('model_id')
        self.train_scenario_id = config.get('train_scenario')
        self.test_scenario_id = config.get('test_scenario')
        self.substation_id = config.get('substation_id')
        self.model_path = self.model_paths.get(self.model_id)
        self.model_infos = config.get('model_infos')
        self.model_info = self.model_infos.get(self.model_id)
        resample_config = self.model_info.get('resample_config')
        resample_config['active_columns'] = self.model_info.get('inputs')
        self.model_info['resample_config'] = resample_config
        self.overwrite_run = config.get('overwrite_run')
        self.matlabeng = mateng
        self.logger_sentence = config.get('logger_sentence')
        self.server_logger = config.get('server_logger')

        self.data_tools = data_tools
        self.check_tools = check_tools
        # Add logging entry
        self.server_logger.warning('{!s} Received at {!s}'.format(self.logger_sentence,
                                                                  config.get('request_timestamp')))

    def eval(self, eval_data):

        # Import model_handle. Create config file. Pass it, along with input data.
        model_handle_path = self.model_path + '/model_handle'
        model_handle = importlib.import_module(model_handle_path.replace('/', '.'), package=None)
        model_config = {'model_id': self.model_id, 'train_scenario': self.train_scenario_id,
                        'substation_id': self.substation_id, 'test_scenario': self.test_scenario_id,
                        'model_path': self.model_path}

        # Run preprocessing steps
        cropped_data = self.data_tools.crop_data(eval_data)
        filtered_data = self.data_tools.filter_data(cropped_data, self.model_info.get('filter_config'))
        resampled_data = self.data_tools.resample_data(filtered_data, self.model_info.get('resample_config'))

        # Pass resampled data and model config to model handle for xy_creation and train evaluation.
        model_handle = model_handle.Model_Handle(model_config, self.matlabeng)
        xy_data = model_handle.xy_creation(resampled_data, False)

        # Check if xy_data are valid (currently just checks if they're nonzero - Feature added for future expansion)
        xy_data_check = CheckTools.xy_data_check(xy_data)
        if xy_data_check == 'xy_data_valid':
            run_id = '{!s}_{!s}_{!s}_{!s}'.format(self.model_id, self.train_scenario_id, self.substation_id,
                                                  self.test_scenario_id)
            try:
                results = model_handle.run_eval(xy_data)
            except RuntimeError:
                error_msg = 'matlab_eval_model_error: {!s}'.format(run_id)
                self.server_logger.exception('{!s} {!s}'.format(self.logger_sentence, error_msg))
                output = error_msg
                return output
            self.server_logger.warning('{!s} eval complete'.format(self.logger_sentence))
            # Modify the results from Dataframe to json
            output = results
            output_metrics = output.dropna()
            mae = round(metrics.mean_absolute_error(output_metrics['yreal'].values, output_metrics['yhat'].values), 4)
            r2 = round(metrics.r2_score(output_metrics['yreal'].values, output_metrics['yhat'].values), 4)
            print('{!s}  MAE {!s}   R2 {!s}'.format(self.logger_sentence, mae, r2))
        else:
            self.server_logger.warning('{!s} eval failed - Zero Data'.format(self.logger_sentence))
            output = 'preprocess_fail_xy_data_zero'

        return output
