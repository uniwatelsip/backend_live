import requests
import pandas as pd
import time


def upload_dataset(url, data_path, name):
    # pass a json file containing 1. The dataset 2. The metadata
    start_time = time.time()
    csv_path = '{!s}.csv'.format(data_path)
    dataset = pd.read_csv(csv_path, low_memory=False)
    dataset = dataset.to_dict(orient='list')
    post_data = {'substation_id': name, 'daterange': "bees", 'dataset': dataset}
    url = "http://{!s}:8081/upload_dataset".format(url)
    r = requests.post(url, json=post_data)
    end_time = time.time()
    print("Dataset uploaded. Time elapsed:", end_time - start_time)
    return r.text


def upload_scenario(url, data_path, name):
    # pass a json file containing 1. The dataset 2. The metadata
    start_time = time.time()
    csv_path = '{!s}.csv'.format(data_path)
    scenario= pd.read_csv(csv_path, low_memory=False)
    scenario = scenario.to_dict(orient='list')
    post_data = {'scenario_id': name, 'daterange': "bees", 'scenario': scenario}
    url = "http://{!s}:8081/upload_scenario".format(url)
    r = requests.post(url, json=post_data)
    end_time = time.time()
    print("Dataset uploaded. Time elapsed:", end_time - start_time)
    return r.text


def train(url, config):
    url = "http://{!s}:8081/train_model".format(url)
    r = requests.get(url, json=config)
    print(r.text)
    return r.text


def eval(url, config):
    url = "http://{!s}:8081/eval_model".format(url)
    r = requests.get(url, json=config)
    response_string = r.text
    try:
        eval_data = pd.read_json(response_string, orient='split')
        print(eval_data.head())
        print('{!s}_{!s}_{!s} evaluated on {!s}'.format(config.get('model_id'), config.get('train_scenario'),
                                                        config.get('substation_id'), config.get('test_scenario')))
    except:
        print(response_string)
    return r


def outlier_detection(url, config):
    import matplotlib.pyplot as plt
    from random import random
    from datetime import datetime
    url = "http://{!s}:8081/outlier_detection".format(url)
    r = requests.get(url, json=config)
    response_string = r.text
    try:
        od_data = pd.read_json(response_string, orient='split')
        print('Outlier Detection Successful')

        outliers = od_data[od_data['outlier_filter'] == 1]
        outliers = outliers['SL_Active']
        outlier_num = outliers.shape[0]
        outlier_percent = round(outlier_num / od_data.shape[0] * 100, 4)
        fig_title = 'Station: {!s} | Daterange: {!s} - {!s} | Method: {!s} | Threshold: {!s} | Window size: {!s} | \n '\
                    'Outliers: {!s} | Percent: {!s} %'.format(config.get('substation_id'),
                                    config.get('date_from'), config.get('date_to'), config.get('od_method'),
                                    config.get('threshold_factor'), config.get('mov_window'), outlier_num, outlier_percent)
        plt.figure(int(random() * 10))
        plt.scatter(od_data.index.values, od_data['SL_Active'].values, c='b', marker='s', s=1, label='raw')
        plt.scatter(outliers.index.values, outliers.values, c='r', marker='s', s=2, label='outlier')
        plt.legend(loc='upper left')
        plt.title(fig_title)
        plt.show()
    except:
        print('Oops, error')


def outlier_detection_multiple(url, configs):
    import matplotlib.pyplot as plt
    from random import random
    from datetime import datetime
    url = "http://{!s}:8081/outlier_detection".format(url)

    all_data = {}
    for i in range(3):
        try:
            config_out = configs[i]
            config_out['substation_id'] = configs['substation_id']
            config_out['date_from'] = configs['date_from']
            config_out['date_to'] = configs['date_to']
            r = requests.get(url, json=config_out)
            response_string = r.text
            all_data[i] = pd.read_json(response_string, orient='split')
        except:
            print('Oops, error')
            quit()

    fig_title_total = 'Station: {!s} | Daterange: {!s} - {!s} |'.format(configs.get('substation_id'),
                                                                       configs.get('date_from'), configs.get('date_to'))

    outliers_all = {}
    for i in range(3):
        outliers = all_data[i][all_data[i]['outlier_filter'] == 1]
        outliers_all[i] = outliers['SL_Active']
        outlier_num = outliers.shape[0]
        outlier_percent = round(outlier_num / all_data[i].shape[0] * 100, 4)
        fig_title_part = 'Method {!s}: {!s} | Threshold: {!s} | Window size: {!s} | Outliers: {!s} | ' \
                         'Percent: {!s} %'.format(i+1, configs[i].get('od_method'), configs[i].get('threshold_factor'),
                                                  configs[i].get('mov_window'), outlier_num, outlier_percent)
        fig_title_total = fig_title_total + '\n' + fig_title_part

    plt.figure(int(random() * 10))
    plt.scatter(all_data[0].index.values, all_data[0]['SL_Active'].values, c='b', marker='.', s=30, label='raw')
    plt.scatter(outliers_all[0].index.values, outliers_all[0].values, c='r', marker='1', s=70, label='method 1')
    plt.scatter(outliers_all[1].index.values, outliers_all[1].values, c='k', marker='+', s=90, label='method 2')
    plt.scatter(outliers_all[2].index.values, outliers_all[2].values, c='g', marker='x', s=70, label='method 3')
    plt.legend(loc='upper left')
    plt.title(fig_title_total)
    plt.show()


def direct_eval(url, config):
    url = "http://{!s}:8081/direct_eval".format(url)
    test_data = pd.read_csv('test_data.csv')
    test_data = test_data.to_dict(orient='list')
    config['test_data'] = test_data
    r = requests.post(url, json=config)
    response_string = r.text
    try:
        eval_data = pd.read_json(response_string, orient='split')
        print(eval_data.head())
    except:
        print(response_string)
    return r


def res_grab(url, config):
    url = "http://{!s}:8081/res_grab".format(url)
    r = requests.get(url, json=config)
    response_string = r.text
    try:
        eval_data = pd.read_json(response_string, orient='split')
        print(eval_data.head())
    except:
        print(response_string)
    return r

def data_grab(url, config):
    # If you want the data to be preprocessed based on the model_id preprocessor, specify preprocess_data: True.
    # This will yield a csv with the following columns: [datetime  model_inputs  yreal]
    # If you just want the merged scenario-dataset, then specify preprocess_data: False
    # This will yield a csv with the following columns: [datetime raw_model_inputs Filter]. The 'Filter' column
    # denotes the scenario values.
    url = "http://{!s}:8081/data_grab".format(url)
    r = requests.get(url, json=config)
    response_string = r.text
    try:
        data = pd.read_json(response_string, orient='split')
        print(data.head())
        filename = 'data_{!s}_{!s}_{!s}.csv'.format(config.get('model_id'),
                                                    config.get('train_scenario_id'), config.get('substation_id'))
        data.to_csv(filename)
    except:
        print(response_string)
    return r

def model_list(url):
    url = "http://{!s}:8081/model_list".format(url)
    r = requests.get(url)
    return r.text


def is_online(url):
    url = "http://{!s}:8081/is_online".format(url)
    r = requests.get(url)
    print(r.text)
    return r.text


if __name__ == "__main__":
    server_url = 'telsipbackend.hopto.org'

    # check trello for models, scenarios and substation codes.
    config = {'model_id': 'm017', 'train_scenario': 'sc032_02', 'substation_id': 'st00046', 'preprocess_data': True}
    data_grab(server_url, config)
