function is_outlier = outlierdetect(input_data, datetime_index, od_method, mov_window, threshold_factor)
    % Outlier detection function

    % The function outlierdetect reads:
    % input_data: Active power data
    % datetime_index: Vector of datetimes for active power data
    % od_method: Outlier detection method (see https://de.mathworks.com/help/matlab/ref/isoutlier.html)
    % threshold_factor: Tuning factor for outlier detection methods, see link
    % mov_window: specifies the size (in minutes) of the moving window in the case of
    % a moving window outlier detection method

    % The function outlierdetect returns:
    % A {0, 1} index vector signifying {inlier, outlier}.
    datetime_index = datetime(datetime_index, 'ConvertFrom', 'posixtime');
    mov_window_methods = {'movmedian', 'movmean'};
    regular_od_methods = {'median', 'mean', 'quartiles', 'grubbs', 'gesd'};
    if any(strcmp(od_method, mov_window_methods))
        is_outlier = isoutlier(input_data, od_method, minutes(mov_window), 'ThresholdFactor', threshold_factor, ...
                               'SamplePoints', datetime_index);
    elseif any(strcmp(od_method, regular_od_methods))
        is_outlier = isoutlier(input_data, od_method, 'ThresholdFactor', threshold_factor, ...
                               'SamplePoints', datetime_index);
    else
        disp('WARNING: unspecified method. Returning zero-vector');
        is_outlier = zeros(1, length(input_data));
    end

end