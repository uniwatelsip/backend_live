===== EMtech - Telsip HTTP Server ====
--------------------------------------
v.1.1

1.____Quick Overview of the program:____

	1.1 The http client (http_client.py) creates a request for the server (server.py). These are:
   		a. /is_online      : Checks if the server is online.
    		   	     
  		b. /upload_dataset : Uploads a dataset to the server via POST. The server saves it in its' local database.
			     
		c. /upload_scenario: Uploads a scenario to the server via post. The server saves it in its' local database.
				
   		d. /train_model    : Uploads a config file to the server and, using the stored datasets & scenarios,
     	                 	     creates a new trained model.
			     
   		e. /eval_model     : Uploads a config file to the server and, using the stored datasets & scenarios,
     	                  	     evaluates an existing trained model.

		f. /direct_eval    : Uploads a config file + dataset to the server and evaluates an existing trained model.
			     		     
   		g. /model_list     : Requests a dictionary containing all directories in the server, so that the
		 		     client can see which models, trained models, datasets and scenarios are available.
		
		h. /res_grab	   : Uploads a config file to the server and returns the requested result .csv.
				

	2.1 In the case of requests (d,e,f), the server initializes the backend (telsipbackend.py), so that:
		- In the case of (d,e), the requested substation data and scenario are merged together in one dataset.
		- This dataset is then preprocessed based on the requested Model ID.
      	 	- A XY table is created
		- Matlab engine is initialized
        	- The data are used to either train a new model or evaluate an existing trained model.
        	- In the case of training (d), the model parameters are saved locally, and a flag is returned.
        	- In the case of evaluation (e), the results are saved locally and also returned to the client.
		- In the case of direct evaluation (f), the results are not saved locally, just returned to the client.


2. ____Structure:____
	When new models are added by TelSiP or new trained models are created, the backend updates its' folder structure, as
   	well as the paths.yaml file. 

	2.1 Folder Structure

  	     v telsip backend
  		   server.py
  		   telsipbackend.py
  		   train.py
  		   eval.py
  		   paths.yaml
 		 v models
		    > m001
  		    v m000
   		     	 model_handle.py
   		     	 modeltrain.m
   		      	 modeleval.m
   		       	 preprocessor.py					
    		       v sc018_02					  # Scenarios
     		     	  v st000005				  	  # Substations
     		  	       parameters.m			          # Parameters of a trained model
			     v res				          # Results of a trained model for given testing scenarios tXXX
       		     	           m000_sc018_02_st0005_t018_02.csv
		 v datasets
		    v data
			 st000000.csv
			 st000002.csv
		    v scenarios
			 018_02.csv
			 017_02.csv

	

	2.2 YAML Structure (example)
		 dataset_paths: {st000000: datasets/data/st000000, st000002: datasets/data/st000002}
		 model_paths: {m000: models/m000, m001: models/m001}
		 scenario_paths: {s017_02: datasets/scenarios/s017_02, s018_02: datasets/scenarios/s018_02}
		 model_info:
		   m000:
		     inputs: ['SL_Active']
		     nan_thres: 0.55
		   m001:
		     inputs: ['SL_Active', 'Wt_Speed', 'Wt_Clouds', 'Wt_Temp']
		     nan_thres: 0.55
		 trained_model_paths:
		   m000:
 		    sc018_02: {st000: models/m000/sc018_02/st000000}
		   m001:
 		    sc017_02: {st000: models/m001/sc017_02/st000000}


3. ____Detailed description of client to server interface for each URL:____

   		a. /is_online      : Accepts: http request
				     Returns: "Server is Online" 
    		   	     
  		b. /upload_dataset : Accepts: post request containing the following json data
					      post_data = {'substation_id': substation_id, 'daterange': "2013-2018", 'dataset': dataset}

					      The variable 'dataset' is a list-oriented dict produced from a pandas dataframe (df.to_dict())
					      containing AT LEAST the following column names:
					      SL_Active, SL_DateTime, SL_Reactive, SL_Voltage, SL_ir3_Tap, Wt_Clouds, Wt_Speed, Wt_Temp
					      
					      The dict looks like this (first three values):
						{ 'SL_Active': [12.47998, 12.99841, 13.09305], 
						  'SL_DateTime': ['2013-11-01 00:00:56', '2013-11-01 00:01:56', '2013-11-01 00:02:58'],
						  'SL_Reactive': [0.64794, 1.5212700000000001, 1.5300799999999999],
						  'SL_Voltage': [20.28758, 20.30765, 20.29498],
						  'SL_ir3_Tap': [2.0, 2.0, 2.0],
						  'Wt_Clouds': [30, 30, 30],
						  'Wt_Speed': [0, 0, 0], 
						  'Wt_Temp': [15.1, 15.1, 15.1] }

					      The user is encouraged to check out http_client.py, where the exact set of commands is
					      demonstrated.
					
 				     Returns: "Dataset Uploaded and stored as 'substation_id' " 
			     
		c. /upload_scenario: Accepts: post request containing the following json data
					      post_data = {'scenario_id': scenario_id, 'daterange': "2013-2018", 'scenario': scenario}

					      The variable 'dataset' is a list-oriented dict produced from a pandas dataframe (df.to_dict())
					      containing the following column names:
					      SL_DateTime, Filter
					      
					      The dict looks like this (first three values):
						{'SL_DateTime': ['2013-11-01 00:00:56', '2013-11-01 00:01:56', '2013-11-01 00:02:58'],
						 'Filter': [False, False, False] }
					      
					      The purpose of the "Filter" column is to highlight the timestamps that we want predictions on. The
					      'SL_DateTime' column can have a fixed or a variable time step - the Backend will still highlight
					      the correct timestamps on the dataset. However, the scenario timestep must not exceed 2 mins!
				     
				     Returns: "Scenario Uploaded and stored as 'scenario_id' "
					      
   		d. /train_model    : Accepts: http request containing the following json data
				              config = {'model_id': "m001", 'train_scenario': "sc018_02", 'substation_id': "st000000", 
							'overwrite_model': False}
					      		Warning! The train scenario code must always be preceded by the "sc" prefix.
							Warning! In case of overwrite_model = True, the server overwrites the trained model directory. 
								 All evaluations for that particular trained model are lost.
					      
				     Returns: - In case of a successful train: "Train Complete"
					      - In case of errors:
							a) 'Trained_Model_ID_exists': A trained model has already been trained with the given info.
							b) 'Model_ID_nonexistent': Such model doesn't exist on database.
			     				c) 'Model_ID_exists__Scenario_ID_nonexistent': Such scenario doesn't exist on database.
							d) 'Model_ID_+_scenario_ID_exist__subst_ID_nonexistent': Such dataset doesn't 
							    exist on database.
							e) 'Supplied_filter_out_of_datetime_bounds': The last true value of the requested scenario
							    is beyond the last value of the requested dataset.
							f) 'nan_percent_over_threshold" :  The NaN percent of the training dataset is over the allowed threshold. The
							    threshold is specified manually by the model creator in the paths.yaml file, and it applies to all inputs of
							    the specific model (that is, columns that will be used to extract model features from - see paths.yaml example above)
							g) 'insufficient_dataframe_length': The length of the training dataset is less than 10, thus indicating
							    that there is something wrong with the data. 
							h) 'preprocess_fail_xy_data_zero': The length of the preprocessed dataset is 0, thus indicating that there
							    is something wrong with the data.

   		e. /eval_model     : Accepts: http request containing the following json data
				              config = {'model_id': "m001", 'train_scenario': "sc018_02", 'substation_id': "st000000",
							'test_scenario': "t018_02", 'overwrite_run': True}
					     		 Warning! The train scenario code must always be preceded by the "sc" prefix.
						      	 	  The test scenario code must always be preceded by the "t" prefix.
							 Warning! The overwrite_run parameter will overwrite any old run in the server, if it exists.
					      
				     Returns: - In case of a successful evaluation: 
							A jsonified pandas dataframe containing the following columns: [yreal, yhat] with a
							datetime index. The jsonified dataframe can be parsed on the client side using the 
							pandas command pd.read_json(). 

							The user is encouraged to check out http_client.py, where the exact set 
							of commands is demonstrated.

					      - In case of errors:
							a) 'Model_ID_nonexistent': No such model ID exists in telsip backend.
							b) 'Model_not_trained_on_subst_ID': Model hasn't been trained on such substation.
							c) 'Model_not_trained_on_scenario': Model hasn't been trained on such scenario.
			     				c) 'Model_ID_exists__Scenario_ID_nonexistent': Such scenario doesn't exist on database.
							d) 'Model_ID_+_scenario_ID_exist__subst_ID_nonexistent': Such dataset doesn't 
							    exist on database.
							e) 'Supplied_filter_out_of_datetime_bounds': The last true value of the requested test 
							    scenario is beyond the last value of the requested dataset.
							f) 'Run_already_exists:~run_id~' : The requested run already exists. Use res_grab to retrieve it.
							g) 'nan_percent_over_90%:' : The NaN percent of the testing dataset is over 90%, thus indicating
							    that there is something wrong with the data.
							h) 'preprocess_fail_xy_data_zero': The length of the preprocessed dataset is 0, thus indicating that there
							    is something wrong with the data.

   		f. /direct_eval     : Accepts: http request containing the following json data
				              config = {'model_id': "m001", 'train_scenario': "sc018_02", 'substation_id': "st000000",
							'test_data': test_data}
					     		 Warning! The train scenario code must always be preceded by the "sc" prefix.
					      
				     Returns: - In case of a successful evaluation: 
							A jsonified pandas dataframe containing the following columns: [yreal, yhat] with a
							datetime index. The jsonified dataframe can be parsed on the client side using the 
							pandas command pd.read_json(). 

							The user is encouraged to check out http_client.py, where the exact set 
							of commands is demonstrated.

					      - In case of errors:
							a) 'Model_ID_nonexistent': No such model ID exists in telsip backend.
							b) 'Model_not_trained_on_subst_ID': Model hasn't been trained on such substation.
							c) 'Model_not_trained_on_scenario': Model hasn't been trained on such scenario.
							d) 'nan_percent_over_90%:' : The NaN percent of the testing dataset is over 90%, thus indicating
							    that there is something wrong with the data.
							e) 'preprocess_fail_xy_data_zero': The length of the preprocessed dataset is 0, thus indicating that there
							    is something wrong with the data.
			     		     
   		g. /model_list     : Accepts: http request
				     Returns: the YAML.path file, as shown in 2.2
		
		h. /res_grab	   : Accepts: http request containing the following json data
					      config = {'model_id': "m001", 'train_scenario': "sc018_02", 'substation_id': "st000000",
							'test_scenario': "t018_02", 'from': "2018-01-01 07:20:00", 'to': "2019-01-01 07:20:00"}
				     Returns: - In case of a successful evaluation:
							The requested result for the given datetime range, as a jsonified pandas dataframe containing
							the following columns: [yreal, yhat] with a datetime index. The jsonified dataframe can be 
							parsed on the client side using the pandas command pd.read_json(). 
					      - In case of errors: 
							a) 'run_nonexistent': In case the run doesn't exist
							b) 'model_not_trained': In case the trained model doesn't exist.
							
4.____Tips + Guidelines:____

	1.  For train and eval requests, the config file must contain the keys as shown in http client. It is also expected to keep the
  	    notation on the content of the config files (that means, model ID is "m000" and not "model000" or w/e)
	2.  Adding a new Model is a manual operation on the server side, requiring that a folder named mXXX is created and the 
	    respective .m and .py files be placed. paths.yaml must also be updated accordingly, which means:
	    - add an entry in model_paths: 
	                                 mXXX: models/mXXX
	    - add an entry in trained_model_paths:
	                                 mXXX: {}
	3. The client should take care not to duplicate trained models. The server will not overwrite the existing ones in its' directory,
	   however it will return an error to the client. The client should inspect the available models and trained models in the 
	   servers directory by requesting model_list, before training a new model.
	4. Any scenario and dataset uploads with the same ID will overwrite the existing ones in the servers' database. 
	   Therefore, the client should take care not to mix up IDs.
	5. The client should follow the nomenclatures' notation when setting IDs for models, scenarios and datasets.
	   